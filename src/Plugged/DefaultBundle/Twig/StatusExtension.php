<?php

namespace Plugged\DefaultBundle\Twig;

class StatusExtension extends \Twig_Extension
{
    public function __construct($container){
        $this->container = $container;
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('Status', array($this, 'StatusFilter')),
        );
    }

    public function StatusFilter($data)
    {
        $choices = [
         '1' => 'Ativo',
         '0' => 'Inativo',
         '2' => 'Meta Atiginda'
        ];
        return $choices[$data];
    }

    public function getName()
    {
        return 'Status';
    }
}
