<?php

namespace Plugged\DefaultBundle\Entity;

/**
 * MetaRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class MetaRepository extends \Doctrine\ORM\EntityRepository
{
	public function getMetaProfissionais($metaId){

		$qb = $this->createQueryBuilder("m");
		$qb->select("p.id");
		$qb->innerJoin("Plugged\DefaultBundle\Entity\AtividadeMeta","am","with","m.id = am.metaId");
		$qb->innerJoin("Plugged\DefaultBundle\Entity\AtividadeMetaProfissional","ap","with","ap.atividadeMetaId = am.id");
		$qb->innerJoin("Plugged\DefaultBundle\Entity\Profissional","p","with","ap.profissionalId = p.id");

		$qb->andWhere("m.id = :id")->setParameter("id",$metaId);

		return $qb->getQuery()->getResult();


	}
}
