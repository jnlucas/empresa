<?php

namespace Plugged\DefaultBundle\Entity;

/**
 * Equipe
 */
class Equipe
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nome;

    /**
     * @var string
     */
    private $descricao;

    /**
     * @var \DateTime
     */
    private $dataCriacao;

    /**
     * @var boolean
     */
    private $status;

    /**
     * @var string
     */
    private $responsavel;

    /**
     * @var integer
     */
    private $empresaId;
    /**
     * @var integer
     */
    private $profissionalId;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Equipe
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     *
     * @return Equipe
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set dataCriacao
     *
     * @param \DateTime $dataCriacao
     *
     * @return Equipe
     */
    public function setDataCriacao($dataCriacao)
    {
        $this->dataCriacao = $dataCriacao;

        return $this;
    }

    /**
     * Get dataCriacao
     *
     * @return \DateTime
     */
    public function getDataCriacao()
    {
        return $this->dataCriacao;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Equipe
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set responsavel
     *
     * @param string $responsavel
     *
     * @return Equipe
     */
    public function setResponsavel($responsavel)
    {
        $this->responsavel = $responsavel;

        return $this;
    }

    /**
     * Get responsavel
     *
     * @return string
     */
    public function getResponsavel()
    {
        return $this->responsavel;
    }
    
        /**
     * Get usuarioId
     *
     * @return integer
     */
    public function getEmpresaId()
    {
        return $this->empresaId;
    }
    
    /**
     * Set usuarioId
     *
     * @param integer $usuarioId
     *
     * @return ProfissionalAvaliacao
     */
    public function setEmpresaId($empresaId)
    {
        $this->empresaId = $empresaId;

        return $this;
    }


       /**
     * Get usuarioId
     *
     * @return integer
     */
    public function getProfissionalId()
    {
        return $this->profissionalId;
    }

    /**
     * Set usuarioId
     *
     * @param integer $usuarioId
     *
     * @return ProfissionalAvaliacao
     */
    public function setProfissionalId($profissionalId)
    {
        $this->profissionalId = $profissionalId;

        return $this;
    }


    public function setDefaultValues(){
        $this->dataCriacao = new \DateTime('now');
        $this->status = true;
        return $this;
    }
}

