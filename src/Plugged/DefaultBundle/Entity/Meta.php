<?php

namespace Plugged\DefaultBundle\Entity;

/**
 * Meta
 */
class Meta
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $descricao;

    /**
     * @var integer
     */
    private $pontos;
    /**
     * @var integer
     */
    private $empresaId;

    /**
     * @var \DateTime
     */
    private $periodo;

    /**
     * @var \DateTime
     */
    private $periodoFim;

    /**
     * @var boolean
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $dataCadastro;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     *
     * @return Meta
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set pontos
     *
     * @param integer $pontos
     *
     * @return Meta
     */
    public function setPontos($pontos)
    {
        $this->pontos = $pontos;

        return $this;
    }

    /**
     * Get pontos
     *
     * @return integer
     */
    public function getPontos()
    {
        return $this->pontos;
    }

    /**
     * Set periodo
     *
     * @param \DateTime $periodo
     *
     * @return Meta
     */
    public function setPeriodo($periodo)
    {
        $this->periodo = $periodo;

        return $this;
    }

    /**
     * Get periodo
     *
     * @return \DateTime
     */
    public function getPeriodo()
    {
        return $this->periodo;
    }

    /**
     * Set periodoFim
     *
     * @param \DateTime $periodoFim
     *
     * @return Meta
     */
    public function setPeriodoFim($periodoFim)
    {
        $this->periodoFim = $periodoFim;

        return $this;
    }

    /**
     * Get periodoFim
     *
     * @return \DateTime
     */
    public function getPeriodoFim()
    {
        return $this->periodoFim;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Meta
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set dataCadastro
     *
     * @param \DateTime $dataCadastro
     *
     * @return Meta
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;

        return $this;
    }

    /**
     * Get dataCadastro
     *
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }
    
        /**
     * Get usuarioId
     *
     * @return integer
     */
    public function getEmpresaId()
    {
        return $this->empresaId;
    }
    
    /**
     * Set usuarioId
     *
     * @param integer $usuarioId
     *
     * @return ProfissionalAvaliacao
     */
    public function setEmpresaId($empresaId)
    {
        $this->empresaId = $empresaId;

        return $this;
    }
    
    public function setDefaultValues(){
        $this->dataCadastro = new \DateTime('now');
        
        return $this;
    }

}

