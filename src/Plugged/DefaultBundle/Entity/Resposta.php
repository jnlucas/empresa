<?php

namespace Plugged\DefaultBundle\Entity;

/**
 * Resposta
 */
class Resposta
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $perguntaId;

    /**
     * @var string
     */
    private $resposta;

    /**
     * @var boolean
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $data;

    private $quantidade;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set perguntaId
     *
     * @param integer $perguntaId
     *
     * @return Resposta
     */
    public function setPerguntaId($perguntaId)
    {
        $this->perguntaId = $perguntaId;

        return $this;
    }

    /**
     * Get perguntaId
     *
     * @return integer
     */
    public function getPerguntaId()
    {
        return $this->perguntaId;
    }

    /**
     * Set resposta
     *
     * @param string $resposta
     *
     * @return Resposta
     */
    public function setResposta($resposta)
    {
        $this->resposta = $resposta;

        return $this;
    }

    /**
     * Get resposta
     *
     * @return string
     */
    public function getResposta()
    {
        return $this->resposta;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Resposta
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return Resposta
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return Resposta
     */
    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;

        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    public function setDefaultValues(){
        $this->data = new \DateTime('now');
        
        return $this;
    }
}

