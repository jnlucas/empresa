<?php

namespace Plugged\DefaultBundle\Entity;

/**
 * RespostaProfissional
 */
class RespostaProfissional
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $perguntaId;

    /**
     * @var integer
     */
    private $respostaId;

    /**
     * @var integer
     */
    private $empresaId;

    /**
     * @var integer
     */
    private $profissionalId;

    /**
     * @var string
     */
    private $observacao;

    /**
     * @var boolean
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $data;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set perguntaId
     *
     * @param integer $perguntaId
     *
     * @return RespostaProfissional
     */
    public function setPerguntaId($perguntaId)
    {
        $this->perguntaId = $perguntaId;

        return $this;
    }

    /**
     * Get perguntaId
     *
     * @return integer
     */
    public function getPerguntaId()
    {
        return $this->perguntaId;
    }

    /**
     * Set respostaId
     *
     * @param integer $respostaId
     *
     * @return RespostaProfissional
     */
    public function setRespostaId($respostaId)
    {
        $this->respostaId = $respostaId;

        return $this;
    }

    /**
     * Get respostaId
     *
     * @return integer
     */
    public function getRespostaId()
    {
        return $this->respostaId;
    }

    /**
     * Set empresaId
     *
     * @param integer $empresaId
     *
     * @return RespostaProfissional
     */
    public function setEmpresaId($empresaId)
    {
        $this->empresaId = $empresaId;

        return $this;
    }

    /**
     * Get empresaId
     *
     * @return integer
     */
    public function getEmpresaId()
    {
        return $this->empresaId;
    }

    /**
     * Set profissionalId
     *
     * @param integer $profissionalId
     *
     * @return RespostaProfissional
     */
    public function setProfissionalId($profissionalId)
    {
        $this->profissionalId = $profissionalId;

        return $this;
    }

    /**
     * Get profissionalId
     *
     * @return integer
     */
    public function getProfissionalId()
    {
        return $this->profissionalId;
    }

    /**
     * Set observacao
     *
     * @param string $observacao
     *
     * @return RespostaProfissional
     */
    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;

        return $this;
    }

    /**
     * Get observacao
     *
     * @return string
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return RespostaProfissional
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return RespostaProfissional
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }
}

