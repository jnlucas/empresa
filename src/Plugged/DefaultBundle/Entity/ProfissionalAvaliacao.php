<?php

namespace Plugged\DefaultBundle\Entity;

/**
 * ProfissionalAvaliacao
 */
class ProfissionalAvaliacao
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $usuarioId;

    /**
     * @var integer
     */
    private $empresaId;
    /**
     * @var integer
     */
    private $profissionalId;
    /**
     * @var \DateTime
     */
    private $data;

    /**
     * @var float
     */
    private $pontuacao;
    /**
     * @var float
     */
    private $pontuacaoPeso;
    /**
     * @var integer
     */
    private $tipoAvaliacaoId;

    /**
     * @var string
     */
    private $superiorImediato;

    /**
     * @var string
     */
    private $observacao;
    /**
     * @var string
     */
    private $metaId;

    /**
     * @var boolean
     */
    private $isPublica;

    /**
     * @var boolean
     */
    private $status;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set usuarioId
     *
     * @param integer $usuarioId
     *
     * @return ProfissionalAvaliacao
     */
    public function setUsuarioId($usuarioId)
    {
        $this->usuarioId = $usuarioId;

        return $this;
    }

    /**
     * Get usuarioId
     *
     * @return integer
     */
    public function getEmpresaId()
    {
        return $this->empresaId;
    }
    
    /**
     * Set usuarioId
     *
     * @param integer $usuarioId
     *
     * @return ProfissionalAvaliacao
     */
    public function setEmpresaId($empresaId)
    {
        $this->empresaId = $empresaId;

        return $this;
    }
    public function getProfissionalId()
    {
        return $this->profissionalId;
    }
    
    /**
     * Set usuarioId
     *
     * @param integer $usuarioId
     *
     * @return ProfissionalAvaliacao
     */
    public function setProfissionalId($profissionalId)
    {
        $this->profissionalId = $profissionalId;

        return $this;
    }
    /**
     * Get usuarioId
     *
     * @return integer
     */
    public function getUsuarioId()
    {
        return $this->usuarioId;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return ProfissionalAvaliacao
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set pontuacao
     *
     * @param float $pontuacao
     *
     * @return ProfissionalAvaliacao
     */
    public function setPontuacao($pontuacao)
    {
        $this->pontuacao = $pontuacao;

        return $this;
    }

    /**
     * Get pontuacao
     *
     * @return float
     */
    public function getPontuacao()
    {
        return $this->pontuacao;
    }

    /**
     * Set pontuacao
     *
     * @param float $pontuacao
     *
     * @return ProfissionalAvaliacao
     */
    public function setPontuacaoPeso($pontuacaoPeso)
    {
        $this->pontuacaoPeso = $pontuacaoPeso;

        return $this;
    }

    /**
     * Get pontuacao
     *
     * @return float
     */
    public function getPontuacaoPeso()
    {
        return $this->pontuacaoPeso;
    }
    /**
     * Set tipoAvaliacaoId
     *
     * @param integer $tipoAvaliacaoId
     *
     * @return ProfissionalAvaliacao
     */
    public function setTipoAvaliacaoId($tipoAvaliacaoId)
    {
        $this->tipoAvaliacaoId = $tipoAvaliacaoId;

        return $this;
    }

    /**
     * Get tipoAvaliacaoId
     *
     * @return integer
     */
    public function getTipoAvaliacaoId()
    {
        return $this->tipoAvaliacaoId;
    }

    /**
     * Set superiorImediato
     *
     * @param string $superiorImediato
     *
     * @return ProfissionalAvaliacao
     */
    public function setSuperiorImediato($superiorImediato)
    {
        $this->superiorImediato = $superiorImediato;

        return $this;
    }

    /**
     * Get superiorImediato
     *
     * @return string
     */
    public function getSuperiorImediato()
    {
        return $this->superiorImediato;
    }

    /**
     * Set observacao
     *
     * @param string $observacao
     *
     * @return ProfissionalAvaliacao
     */
    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;

        return $this;
    }

    /**
     * Get observacao
     *
     * @return string
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * Set isPublica
     *
     * @param boolean $isPublica
     *
     * @return ProfissionalAvaliacao
     */
    public function setIsPublica($isPublica)
    {
        $this->isPublica = $isPublica;

        return $this;
    }

    /**
     * Get isPublica
     *
     * @return boolean
     */
    public function getIsPublica()
    {
        return $this->isPublica;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return ProfissionalAvaliacao
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }
    
        /**
     * Set metaId
     *
     * @param integer $metaId
     *
     * @return AtividadeMeta
     */
    public function setMetaId($metaId)
    {
        $this->metaId = $metaId;

        return $this;
    }

    /**
     * Get metaId
     *
     * @return integer
     */
    public function getMetaId()
    {
        return $this->metaId;
    }
}

