<?php

namespace Plugged\DefaultBundle\Entity;

/**
 * AtividadeMeta
 */
class AtividadeMeta
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $descricao;

    /**
     * @var integer
     */
    private $protocolo;

    /**
     * @var integer
     */
    private $pontos;

    /**
     * @var \DateTime
     */
    private $vencimento;

    /**
     * @var integer
     */
    private $metaId;

    /**
     * @var \DateTime
     */
    private $dataCadastro;

    /**
     * @var boolean
     */
    private $status;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     *
     * @return AtividadeMeta
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set protocolo
     *
     * @param integer $protocolo
     *
     * @return AtividadeMeta
     */
    public function setProtocolo($protocolo)
    {
        $this->protocolo = $protocolo;

        return $this;
    }

    /**
     * Get protocolo
     *
     * @return integer
     */
    public function getProtocolo()
    {
        return $this->protocolo;
    }

    /**
     * Set pontos
     *
     * @param integer $pontos
     *
     * @return AtividadeMeta
     */
    public function setPontos($pontos)
    {
        $this->pontos = $pontos;

        return $this;
    }

    /**
     * Get pontos
     *
     * @return integer
     */
    public function getPontos()
    {
        return $this->pontos;
    }

    /**
     * Set vencimento
     *
     * @param \DateTime $vencimento
     *
     * @return AtividadeMeta
     */
    public function setVencimento($vencimento)
    {
        $this->vencimento = $vencimento;

        return $this;
    }

    /**
     * Get vencimento
     *
     * @return \DateTime
     */
    public function getVencimento()
    {
        return $this->vencimento;
    }

    /**
     * Set metaId
     *
     * @param integer $metaId
     *
     * @return AtividadeMeta
     */
    public function setMetaId($metaId)
    {
        $this->metaId = $metaId;

        return $this;
    }

    /**
     * Get metaId
     *
     * @return integer
     */
    public function getMetaId()
    {
        return $this->metaId;
    }

    /**
     * Set dataCadastro
     *
     * @param \DateTime $dataCadastro
     *
     * @return AtividadeMeta
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;

        return $this;
    }

    /**
     * Get dataCadastro
     *
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return AtividadeMeta
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    public function setDefaultValues(){
        $this->dataCadastro = new \DateTime('now');
        return $this;
    }
}

