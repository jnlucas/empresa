<?php

namespace Plugged\DefaultBundle\Entity;

/**
 * EquipeProfissional
 */
class EquipeProfissional
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $equipeId;

    /**
     * @var integer
     */
    private $profissionalId;

    /**
     * @var integer
     */
    private $empresaId;

    /**
     * @var boolean
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $dataCadastro;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set equipeId
     *
     * @param integer $equipeId
     *
     * @return EquipeProfissional
     */
    public function setEquipeId($equipeId)
    {
        $this->equipeId = $equipeId;

        return $this;
    }

    /**
     * Get equipeId
     *
     * @return integer
     */
    public function getEquipeId()
    {
        return $this->equipeId;
    }

    /**
     * Set profissionalId
     *
     * @param integer $profissionalId
     *
     * @return EquipeProfissional
     */
    public function setProfissionalId($profissionalId)
    {
        $this->profissionalId = $profissionalId;

        return $this;
    }

    /**
     * Get profissionalId
     *
     * @return integer
     */
    public function getProfissionalId()
    {
        return $this->profissionalId;
    }

    /**
     * Set empresaId
     *
     * @param integer $empresaId
     *
     * @return EquipeProfissional
     */
    public function setEmpresaId($empresaId)
    {
        $this->empresaId = $empresaId;

        return $this;
    }

    /**
     * Get empresaId
     *
     * @return integer
     */
    public function getEmpresaId()
    {
        return $this->empresaId;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return EquipeProfissional
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set dataCadastro
     *
     * @param \DateTime $dataCadastro
     *
     * @return EquipeProfissional
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;

        return $this;
    }

    /**
     * Get dataCadastro
     *
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    public function setDefaultValues(){
        $this->dataCadastro = new \DateTime('now');
        $this->status = true;
        return $this;
    }
}

