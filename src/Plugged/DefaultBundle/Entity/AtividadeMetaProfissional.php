<?php

namespace Plugged\DefaultBundle\Entity;

/**
 * AtividadeMetaProfissional
 */
class AtividadeMetaProfissional
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $profissionalId;

    /**
     * @var integer
     */
    private $atividadeMetaId;

    /**
     * @var boolean
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $dataCadastro;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set profissionalId
     *
     * @param integer $profissionalId
     *
     * @return AtividadeMetaProfissional
     */
    public function setProfissionalId($profissionalId)
    {
        $this->profissionalId = $profissionalId;

        return $this;
    }

    /**
     * Get profissionalId
     *
     * @return integer
     */
    public function getProfissionalId()
    {
        return $this->profissionalId;
    }

    /**
     * Set atividadeMetaId
     *
     * @param integer $atividadeMetaId
     *
     * @return AtividadeMetaProfissional
     */
    public function setAtividadeMetaId($atividadeMetaId)
    {
        $this->atividadeMetaId = $atividadeMetaId;

        return $this;
    }

    /**
     * Get atividadeMetaId
     *
     * @return integer
     */
    public function getAtividadeMetaId()
    {
        return $this->atividadeMetaId;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return AtividadeMetaProfissional
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set dataCadastro
     *
     * @param \DateTime $dataCadastro
     *
     * @return AtividadeMetaProfissional
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;

        return $this;
    }

    /**
     * Get dataCadastro
     *
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }
    
    public function setDefaultValues(){
        $this->dataCadastro = new \DateTime('now');
        return $this;
    }
}

