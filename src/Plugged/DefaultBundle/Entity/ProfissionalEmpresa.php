<?php

namespace Plugged\DefaultBundle\Entity;

/**
 * ProfissionalEmpresa
 */
class ProfissionalEmpresa
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $profissionalId;
    /**
     * @var integer
     */
    private $ocupacaoId;

    /**
     * @var integer
     */
    private $empresaId;

    /**
     * @var string
     */
    private $tempoTrabalhado;

    /**
     * @var \DateTime
     */
    private $dataSaida;

    /**
     * @var boolean
     */
    private $isAtual;

    /**
     * @var \DateTime
     */
    private $dataCadastro;

    /**
     * @var boolean
     */
    private $status;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set profissionalId
     *
     * @param integer $profissionalId
     *
     * @return ProfissionalEmpresa
     */
    public function setProfissionalId($profissionalId)
    {
        $this->profissionalId = $profissionalId;

        return $this;
    }

    /**
     * Get profissionalId
     *
     * @return integer
     */
    public function getProfissionalId()
    {
        return $this->profissionalId;
    }
    /**
     * Set profissionalId
     *
     * @param integer $profissionalId
     *
     * @return ProfissionalEmpresa
     */
    public function setOcupacaoId($ocupacaoId)
    {
        $this->ocupacaoId = $ocupacaoId;

        return $this;
    }

    /**
     * Get profissionalId
     *
     * @return integer
     */
    public function getOcupacaoId()
    {
        return $this->ocupacaoId;
    }

    /**
     * Set empresaId
     *
     * @param integer $empresaId
     *
     * @return ProfissionalEmpresa
     */
    public function setEmpresaId($empresaId)
    {
        $this->empresaId = $empresaId;

        return $this;
    }

    /**
     * Get empresaId
     *
     * @return integer
     */
    public function getEmpresaId()
    {
        return $this->empresaId;
    }

    /**
     * Set tempoTrabalhado
     *
     * @param string $tempoTrabalhado
     *
     * @return ProfissionalEmpresa
     */
    public function setTempoTrabalhado($tempoTrabalhado)
    {
        $this->tempoTrabalhado = $tempoTrabalhado;

        return $this;
    }

    /**
     * Get tempoTrabalhado
     *
     * @return string
     */
    public function getTempoTrabalhado()
    {
        return $this->tempoTrabalhado;
    }

    /**
     * Set dataSaida
     *
     * @param \DateTime $dataSaida
     *
     * @return ProfissionalEmpresa
     */
    public function setDataSaida($dataSaida)
    {
        $this->dataSaida = $dataSaida;

        return $this;
    }

    /**
     * Get dataSaida
     *
     * @return \DateTime
     */
    public function getDataSaida()
    {
        return $this->dataSaida;
    }

    /**
     * Set isAtual
     *
     * @param boolean $isAtual
     *
     * @return ProfissionalEmpresa
     */
    public function setIsAtual($isAtual)
    {
        $this->isAtual = $isAtual;

        return $this;
    }

    /**
     * Get isAtual
     *
     * @return boolean
     */
    public function getIsAtual()
    {
        return $this->isAtual;
    }

    /**
     * Set dataCadastro
     *
     * @param \DateTime $dataCadastro
     *
     * @return ProfissionalEmpresa
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;

        return $this;
    }

    /**
     * Get dataCadastro
     *
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return ProfissionalEmpresa
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function setDefaultValues(){
        $this->dataCadastro = new \DateTime('now');
        $this->status = true;
        $this->isAtual = true;
        
        return $this;
    }
}

