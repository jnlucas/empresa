<?php

namespace Plugged\DefaultBundle\Entity;

/**
 * Importacao
 */
class Importacao
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $protocolo;

    /**
     * @var string
     */
    private $atividade;

    /**
     * @var string
     */
    private $profissional;

    /**
     * @var \DateTime
     */
    private $vencimento;

    /**
     * @var \DateTime
     */
    private $inclusao;

    /**
     * @var string
     */
    private $meta;

    /**
     * @var string
     */
    private $situacao;

    /**
     * @var string
     */
    private $pontos;

    /**
     * @var \DateTime
     */
    private $dataInclusao;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set protocolo
     *
     * @param string $protocolo
     *
     * @return Importacao
     */
    public function setProtocolo($protocolo)
    {
        $this->protocolo = $protocolo;

        return $this;
    }

    /**
     * Get protocolo
     *
     * @return string
     */
    public function getProtocolo()
    {
        return $this->protocolo;
    }

    /**
     * Set atividade
     *
     * @param string $atividade
     *
     * @return Importacao
     */
    public function setAtividade($atividade)
    {
        $this->atividade = $atividade;

        return $this;
    }

    /**
     * Get atividade
     *
     * @return string
     */
    public function getAtividade()
    {
        return $this->atividade;
    }

    /**
     * Set profissional
     *
     * @param string $profissional
     *
     * @return Importacao
     */
    public function setProfissional($profissional)
    {
        $this->profissional = $profissional;

        return $this;
    }

    /**
     * Get profissional
     *
     * @return string
     */
    public function getProfissional()
    {
        return $this->profissional;
    }

    /**
     * Set vencimento
     *
     * @param \DateTime $vencimento
     *
     * @return Importacao
     */
    public function setVencimento($vencimento)
    {
        $this->vencimento = $vencimento;

        return $this;
    }

    /**
     * Get vencimento
     *
     * @return \DateTime
     */
    public function getVencimento()
    {
        return $this->vencimento;
    }

    /**
     * Set inclusao
     *
     * @param \DateTime $inclusao
     *
     * @return Importacao
     */
    public function setInclusao($inclusao)
    {
        $this->inclusao = $inclusao;

        return $this;
    }

    /**
     * Get inclusao
     *
     * @return \DateTime
     */
    public function getInclusao()
    {
        return $this->inclusao;
    }

    /**
     * Set meta
     *
     * @param string $meta
     *
     * @return Importacao
     */
    public function setMeta($meta)
    {
        $this->meta = $meta;

        return $this;
    }

    /**
     * Get meta
     *
     * @return string
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * Set situacao
     *
     * @param string $situacao
     *
     * @return Importacao
     */
    public function setSituacao($situacao)
    {
        $this->situacao = $situacao;

        return $this;
    }

    /**
     * Get situacao
     *
     * @return string
     */
    public function getSituacao()
    {
        return $this->situacao;
    }

    /**
     * Set pontos
     *
     * @param string $pontos
     *
     * @return Importacao
     */
    public function setPontos($pontos)
    {
        $this->pontos = $pontos;

        return $this;
    }

    /**
     * Get pontos
     *
     * @return string
     */
    public function getPontos()
    {
        return $this->pontos;
    }

    /**
     * Set dataInclusao
     *
     * @param \DateTime $dataInclusao
     *
     * @return Importacao
     */
    public function setDataInclusao($dataInclusao)
    {
        $this->dataInclusao = $dataInclusao;

        return $this;
    }

    /**
     * Get dataInclusao
     *
     * @return \DateTime
     */
    public function getDataInclusao()
    {
        return $this->dataInclusao;
    }
}

