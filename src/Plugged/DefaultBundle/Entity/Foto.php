<?php

namespace Plugged\DefaultBundle\Entity;

/**
 * Foto
 */
class Foto
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $foto;

    /**
     * @var string
     */
    private $data;

    /**
     * @var Plugged/DefaultBundle/Entity/Anuncio
     */
    private $anuncioId;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set foto
     *
     * @param string $foto
     *
     * @return Foto
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;

        return $this;
    }

    /**
     * Get foto
     *
     * @return string
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * Set data
     *
     * @param string $data
     *
     * @return Foto
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set anuncioId
     *
     * @param integer $anuncioId
     *
     * @return Foto
     */
    public function setAnuncioId($anuncioId)
    {
        $this->anuncioId = $anuncioId;

        return $this;
    }

    /**
     * Get anuncioId
     *
     * @return integer
     */
    public function getAnuncioId()
    {
        return $this->anuncioId;
    }
}

