<?php

namespace Plugged\DefaultBundle\Entity;

/**
 * Quiz
 */
class Quiz
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $pergunta;

    /**
     * @var boolean
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $data;

    /**
     * @var integer
     */
    private $empresaId;

    /**
     * @var integer
     */
    private $respostas;




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pergunta
     *
     * @param string $pergunta
     *
     * @return Quiz
     */
    public function setPergunta($pergunta)
    {
        $this->pergunta = $pergunta;

        return $this;
    }

    /**
     * Get pergunta
     *
     * @return string
     */
    public function getPergunta()
    {
        return $this->pergunta;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Quiz
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return Quiz
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set empresaId
     *
     * @param integer $empresaId
     *
     * @return Quiz
     */
    public function setEmpresaId($empresaId)
    {
        $this->empresaId = $empresaId;

        return $this;
    }

    /**
     * Get empresaId
     *
     * @return integer
     */
    public function getEmpresaId()
    {
        return $this->empresaId;
    }

    /**
     * Set empresaId
     *
     * @param integer $empresaId
     *
     * @return Quiz
     */
    public function setRespostas($respostas)
    {
        $this->respostas = $respostas;

        return $this;
    }

    /**
     * Get empresaId
     *
     * @return integer
     */
    public function getRespostas()
    {
        return $this->respostas;
    }


    public function setDefaultValues(){
        $this->data = new \DateTime('now');
        
        return $this;
    }
}

