<?php

namespace Plugged\DefaultBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Plugged\DefaultBundle\Entity\AtividadeMetaProfissional;
use Plugged\DefaultBundle\Entity\Importacao;

use Plugged\DefaultBundle\Form\AtividadeMetaProfissionalType;

/**
 * AtividadeMetaProfissional controller.
 *
 */
class AtividadeMetaProfissionalController extends Controller
{

    /**
     * Lists all AtividadeMetaProfissional entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('DefaultBundle:AtividadeMetaProfissional')->findAll();

        return $this->render('DefaultBundle:AtividadeMetaProfissional:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new AtividadeMetaProfissional entity.
     *
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = new AtividadeMetaProfissional();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);
        $atividade = $em->getRepository('DefaultBundle:AtividadeMeta')->find($entity->getAtividadeMetaId());
        $meta = $em->getRepository('DefaultBundle:Meta')->find($atividade->getMetaId());

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();
            return $this->redirect($this->generateUrl('atividademetaprofissional_show', array('id' => $entity->getAtividadeMetaId()->getId())));
        }

        return $this->render('DefaultBundle:AtividadeMetaProfissional:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'empresa'      => $empresa,
            'atividade'      => $atividade,
            'meta'      => $meta,
        ));
    }

    /**
     * Creates a new AtividadeMetaProfissional entity.
     *
     */
    public function importAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);

        return $this->render('DefaultBundle:AtividadeMetaProfissional:import.html.twig', array(
            'empresa'      => $empresa,
        ));
    }

    /**
     * Creates a new AtividadeMetaProfissional entity.
     *
     */
    public function importCreateAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);

        if($request->getMethod() == 'POST'){

            $arquivo = (file_get_contents($_FILES['arquivo']['tmp_name']));

            $arquivo = explode("\n",$arquivo);

            foreach($arquivo as $k => $value){
                $linha[$k] = explode(",",$value);
                foreach($linha[$k] as $i => $valores){
                    $import[$k][str_replace("\r","",$linha[0][$i])] = str_replace("\r","",$valores);
                }
            }

            $parametrosImportados = array(
                "protocolo" => "",
                "atividade" => "",
                "profissional" => "",
                "inclusao" => "",
                "vencimento" => "",
                "meta" => "",
                "situacao" => "",
                "pontos" => "",

            );
            unset($import[0]);

            foreach($import as $i => $array){
                foreach($parametrosImportados as $k => $v){
                    if(!isset($array[$k])){

                        unset($import[$i]);

                    }
                }
            }



            foreach($import as $valores ){
                $importacao = new Importacao();

                $inclusao = implode("-",array_reverse(explode("/",$valores['inclusao']))) ;
                $vencimento = implode("-",array_reverse(explode("/",$valores['vencimento'])));
                $importacao->setProtocolo($valores['protocolo'])
                ->setAtividade($valores['atividade'])
                ->setProfissional($valores['profissional'])
                ->setInclusao(new \DateTime($inclusao))
                ->setVencimento(new \DateTime($vencimento))
                ->setMeta($valores['meta'])
                ->setSituacao($valores['situacao'])
                ->setPontos($valores['pontos'])
                ->setDataInclusao(new \DateTime("now"));

                $importacoes[] = $importacao;

                $em->persist($importacao);

            }

            $em->flush();


            $this->get("score")->processaScore();

            $request->getSession()->getFlashBag()->add('success', 'Dados importados com sucesso!');

            return $this->redirect($this->generateUrl("atividademetaprofissional_import"));



        }

        return $this->render('DefaultBundle:AtividadeMetaProfissional:import.html.twig', array(
            'empresa'      => $empresa,
        ));
    }

    /**
     * Creates a form to create a AtividadeMetaProfissional entity.
     *
     * @param AtividadeMetaProfissional $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(AtividadeMetaProfissional $entity)
    {
        $form = $this->createForm(new AtividadeMetaProfissionalType(), $entity, array(
            'action' => $this->generateUrl('atividademetaprofissional_create'),
            'method' => 'POST',
        ));


        return $form;
    }

    /**
     * Displays a form to create a new AtividadeMetaProfissional entity.
     *
     */
    public function newAction($id)
    {
         $em = $this->getDoctrine()->getManager();
        $entity = new AtividadeMetaProfissional();

        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);
        $atividade = $em->getRepository('DefaultBundle:AtividadeMeta')->find($id);
        $meta = $em->getRepository('DefaultBundle:Meta')->find($atividade->getMetaId());

        $entity->setAtividadeMetaId($atividade);

        $form   = $this->createCreateForm($entity);

        return $this->render('DefaultBundle:AtividadeMetaProfissional:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'empresa'      => $empresa,
            'atividade'      => $atividade,
            'meta'      => $meta,
        ));
    }

    /**
     * Finds and displays a AtividadeMetaProfissional entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);
        $atividade = $em->getRepository('DefaultBundle:AtividadeMeta')->find($id);
        $meta = $em->getRepository('DefaultBundle:Meta')->find($atividade->getMetaId());
        $entities = $em->getRepository('DefaultBundle:AtividadeMetaProfissional')->findBy(['atividadeMetaId' => $id]);


        return $this->render('DefaultBundle:AtividadeMetaProfissional:show.html.twig', array(
            'empresa'      => $empresa,
            'atividade'      => $atividade,
            'meta'      => $meta,
            'entities' => $entities,
        ));
    }

    /**
     * Displays a form to edit an existing AtividadeMetaProfissional entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);
        $entity = $em->getRepository('DefaultBundle:AtividadeMetaProfissional')->find($id);
        $atividade = $em->getRepository('DefaultBundle:AtividadeMeta')->find($entity->getAtividadeMetaId());
        $meta = $em->getRepository('DefaultBundle:Meta')->find($entity->getAtividadeMetaId()->getMetaId());

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AtividadeMetaProfissional entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('DefaultBundle:AtividadeMetaProfissional:edit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'empresa'      => $empresa,
            'atividade'      => $atividade,
            'meta'      => $meta,
        ));
    }

    /**
    * Creates a form to edit a AtividadeMetaProfissional entity.
    *
    * @param AtividadeMetaProfissional $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(AtividadeMetaProfissional $entity)
    {
        $form = $this->createForm(new AtividadeMetaProfissionalType(), $entity, array(
            'action' => $this->generateUrl('atividademetaprofissional_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        return $form;
    }
    /**
     * Edits an existing AtividadeMetaProfissional entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:AtividadeMetaProfissional')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AtividadeMetaProfissional entity.');
        }

        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);
        $atividade = $em->getRepository('DefaultBundle:AtividadeMeta')->find($entity->getAtividadeMetaId());
        $meta = $em->getRepository('DefaultBundle:Meta')->find($entity->getAtividadeMetaId()->getMetaId());

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('atividademetaprofissional_show', array('id' => $entity->getAtividadeMetaId()->getId())));
        }

        return $this->render('DefaultBundle:AtividadeMetaProfissional:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'empresa'      => $empresa,
            'atividade'      => $atividade,
            'meta'      => $meta,
        ));
    }
    /**
     * Deletes a AtividadeMetaProfissional entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('DefaultBundle:AtividadeMetaProfissional')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find AtividadeMetaProfissional entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('atividademetaprofissional'));
    }

    /**
     * Creates a form to delete a AtividadeMetaProfissional entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('atividademetaprofissional_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
