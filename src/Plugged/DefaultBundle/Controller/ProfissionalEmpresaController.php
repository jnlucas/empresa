<?php

namespace Plugged\DefaultBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Plugged\DefaultBundle\Entity\ProfissionalEmpresa;
use Plugged\DefaultBundle\Form\ProfissionalEmpresaType;

/**
 * ProfissionalEmpresa controller.
 *
 */
class ProfissionalEmpresaController extends Controller
{

    /**
     * Lists all ProfissionalEmpresa entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);
        
        $entities = $em->getRepository('DefaultBundle:ProfissionalEmpresa')->findAll();

        return $this->render('DefaultBundle:ProfissionalEmpresa:index.html.twig', array(
            'entities' => $entities,
            'empresa' => $empresa,
        ));
    }
    /**
     * Creates a new ProfissionalEmpresa entity.
     *
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);
        $entity = new ProfissionalEmpresa();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('profissional'));
        }

        return $this->render('DefaultBundle:ProfissionalEmpresa:new.html.twig', array(
            'entity' => $entity,
            'empresa' => $empresa,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a ProfissionalEmpresa entity.
     *
     * @param ProfissionalEmpresa $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ProfissionalEmpresa $entity)
    {
        $form = $this->createForm(new ProfissionalEmpresaType(), $entity, array(
            'action' => $this->generateUrl('profissionalempresa_create'),
            'method' => 'POST',
        ));

        
        return $form;
    }

    /**
     * Displays a form to create a new ProfissionalEmpresa entity.
     *
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = new ProfissionalEmpresa();
        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);
        
        $form   = $this->createCreateForm($entity);

        return $this->render('DefaultBundle:ProfissionalEmpresa:new.html.twig', array(
            'entity' => $entity,
            'empresa' => $empresa,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ProfissionalEmpresa entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:ProfissionalEmpresa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProfissionalEmpresa entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('DefaultBundle:ProfissionalEmpresa:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ProfissionalEmpresa entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:ProfissionalEmpresa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProfissionalEmpresa entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('DefaultBundle:ProfissionalEmpresa:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a ProfissionalEmpresa entity.
    *
    * @param ProfissionalEmpresa $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(ProfissionalEmpresa $entity)
    {
        $form = $this->createForm(new ProfissionalEmpresaType(), $entity, array(
            'action' => $this->generateUrl('profissionalempresa_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing ProfissionalEmpresa entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:ProfissionalEmpresa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProfissionalEmpresa entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('profissionalempresa_edit', array('id' => $id)));
        }

        return $this->render('DefaultBundle:ProfissionalEmpresa:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a ProfissionalEmpresa entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('DefaultBundle:ProfissionalEmpresa')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ProfissionalEmpresa entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('profissionalempresa'));
    }

    /**
     * Creates a form to delete a ProfissionalEmpresa entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('profissionalempresa_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
