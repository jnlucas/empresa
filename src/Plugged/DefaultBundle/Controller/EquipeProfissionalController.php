<?php

namespace Plugged\DefaultBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Plugged\DefaultBundle\Entity\EquipeProfissional;
use Plugged\DefaultBundle\Form\EquipeProfissionalType;

/**
 * EquipeProfissional controller.
 *
 */
class EquipeProfissionalController extends Controller
{

    /**
     * Lists all EquipeProfissional entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('DefaultBundle:EquipeProfissional')->findAll();

        return $this->render('DefaultBundle:EquipeProfissional:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new EquipeProfissional entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new EquipeProfissional();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('equipeprofissional_show', array('id' => $entity->getId())));
        }

        return $this->render('DefaultBundle:EquipeProfissional:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a EquipeProfissional entity.
     *
     * @param EquipeProfissional $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(EquipeProfissional $entity)
    {
        $form = $this->createForm(new EquipeProfissionalType(), $entity, array(
            'action' => $this->generateUrl('equipeprofissional_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new EquipeProfissional entity.
     *
     */
    public function newAction()
    {
        $entity = new EquipeProfissional();
        $form   = $this->createCreateForm($entity);

        return $this->render('DefaultBundle:EquipeProfissional:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a EquipeProfissional entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:EquipeProfissional')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EquipeProfissional entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('DefaultBundle:EquipeProfissional:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing EquipeProfissional entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:EquipeProfissional')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EquipeProfissional entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('DefaultBundle:EquipeProfissional:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a EquipeProfissional entity.
    *
    * @param EquipeProfissional $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(EquipeProfissional $entity)
    {
        $form = $this->createForm(new EquipeProfissionalType(), $entity, array(
            'action' => $this->generateUrl('equipeprofissional_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing EquipeProfissional entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:EquipeProfissional')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EquipeProfissional entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('equipeprofissional_edit', array('id' => $id)));
        }

        return $this->render('DefaultBundle:EquipeProfissional:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a EquipeProfissional entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('DefaultBundle:EquipeProfissional')->find($id);
        $equipe = $em->getRepository('DefaultBundle:Equipe')->find($entity->getEquipeId()->getId());

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EquipeProfissional entity.');
        }

        try{
            $em->remove($entity);
            $em->flush();
            $request->getSession()->getFlashBag()->add('success', "Equipe Atualizada com sucesso!");
            return $this->redirect($this->generateUrl("equipe_edit",array('id' => $equipe->getId())));
        }catch(\Exception $e){
            $request->getSession()->getFlashBag()->add('danger', "Erro ao atualizar equipe!");
            return $this->redirect($this->generateUrl("equipe_edit",array('id' => $equipe->getId())));
        }
        $request->getSession()->getFlashBag()->add('danger', "Erro ao atualizar equipe!");
        return $this->redirect($this->generateUrl("equipe_edit",array('id' => $equipe->getId())));
    }

    /**
     * Creates a form to delete a EquipeProfissional entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('equipeprofissional_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
