<?php

namespace Plugged\DefaultBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Plugged\DefaultBundle\Entity\Meta;
use Plugged\DefaultBundle\Form\MetaType;

/**
 * Meta controller.
 *
 */
class MetaController extends Controller
{

    /**
     * Lists all Meta entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();


        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);


        $entities = $em->getRepository('DefaultBundle:Meta')->findBy(array('empresaId' => $empresa));

        return $this->render('DefaultBundle:Meta:index.html.twig', array(
            'entities' => $entities,
            'empresa' => $empresa
        ));
    }
    /**
     * Creates a new Meta entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Meta();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
            $em = $this->getDoctrine()->getManager();
            $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);
        if ($form->isValid()) {

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('meta'));
        }

        return $this->render('DefaultBundle:Meta:new.html.twig', array(
            'entity' => $entity,
            'empresa' => $empresa,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Meta entity.
     *
     * @param Meta $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Meta $entity)
    {
        $form = $this->createForm(new MetaType(), $entity, array(
            'action' => $this->generateUrl('meta_create'),
            'method' => 'POST',
        ));



        return $form;
    }

    /**
     * Displays a form to create a new Meta entity.
     *
     */
    public function newAction()
    {
        $entity = new Meta();
        $em = $this->getDoctrine()->getManager();
        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);
        $form   = $this->createCreateForm($entity);

        return $this->render('DefaultBundle:Meta:new.html.twig', array(
            'entity' => $entity,
            'empresa' => $empresa,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Meta entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:Meta')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Meta entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('DefaultBundle:Meta:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Meta entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:Meta')->find($id);
        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Meta entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('DefaultBundle:Meta:edit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'empresa' => $empresa
        ));
    }

    /**
     * Displays a form to edit an existing Meta entity.
     *
     */
    public function rankingAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();

        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);

        $profissionaisMeta = $this->getDoctrine()->getRepository("DefaultBundle:Meta")->getMetaProfissionais($id);


        $pontosProfissionais = $this->get("score")->getScore($request,$profissionaisMeta[0]["id"],$id);



        return $this->render('DefaultBundle:Meta:ranking.html.twig', array(
            'pontosProfissionais'      => $pontosProfissionais,
            'empresa' => $empresa

        ));
    }

    /**
    * Creates a form to edit a Meta entity.
    *
    * @param Meta $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Meta $entity)
    {
        $form = $this->createForm(new MetaType(), $entity, array(
            'action' => $this->generateUrl('meta_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        return $form;
    }
    /**
     * Edits an existing Meta entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);


        $entity = $em->getRepository('DefaultBundle:Meta')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Meta entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);

        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('meta'));
        }

        return $this->render('DefaultBundle:Meta:edit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'empresa' => $empresa
        ));
    }
    /**
     * Deletes a Meta entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('DefaultBundle:Meta')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Meta entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('meta'));
    }

    /**
     * Creates a form to delete a Meta entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('meta_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
