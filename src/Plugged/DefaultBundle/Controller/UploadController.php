<?php

namespace Plugged\DefaultBundle\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UploadController extends Controller
{
    public function indexAction()
    {
        $upload = $this->get('upload');
        if($_FILES['upload']){
            $diretorio = $this->get('kernel')->getRootDir() . '/../src/Plugged/DefaultBundle/Resources/public/uploads/';
            $imagem = $upload->Envia_Arquivo($_FILES['upload'],time(),500,500,$diretorio, true);

        }
        
        $return = array(
            'foto' => $imagem,
        );

        return new JsonResponse($return);
    }
    
    public function testeAction()
    {
        return $this->render('DefaultBundle:Upload:teste.html.twig', array(
            
        ));
    }

}
