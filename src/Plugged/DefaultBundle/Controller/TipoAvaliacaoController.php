<?php

namespace Plugged\DefaultBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Plugged\DefaultBundle\Entity\TipoAvaliacao;
use Plugged\DefaultBundle\Form\TipoAvaliacaoType;

/**
 * TipoAvaliacao controller.
 *
 */
class TipoAvaliacaoController extends Controller
{

    /**
     * Lists all TipoAvaliacao entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('DefaultBundle:TipoAvaliacao')->findAll();

        return $this->render('DefaultBundle:TipoAvaliacao:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new TipoAvaliacao entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new TipoAvaliacao();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tipoavaliacao_show', array('id' => $entity->getId())));
        }

        return $this->render('DefaultBundle:TipoAvaliacao:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a TipoAvaliacao entity.
     *
     * @param TipoAvaliacao $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TipoAvaliacao $entity)
    {
        $form = $this->createForm(new TipoAvaliacaoType(), $entity, array(
            'action' => $this->generateUrl('tipoavaliacao_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new TipoAvaliacao entity.
     *
     */
    public function newAction()
    {
        $entity = new TipoAvaliacao();
        $form   = $this->createCreateForm($entity);

        return $this->render('DefaultBundle:TipoAvaliacao:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TipoAvaliacao entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:TipoAvaliacao')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoAvaliacao entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('DefaultBundle:TipoAvaliacao:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing TipoAvaliacao entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:TipoAvaliacao')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoAvaliacao entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('DefaultBundle:TipoAvaliacao:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a TipoAvaliacao entity.
    *
    * @param TipoAvaliacao $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TipoAvaliacao $entity)
    {
        $form = $this->createForm(new TipoAvaliacaoType(), $entity, array(
            'action' => $this->generateUrl('tipoavaliacao_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing TipoAvaliacao entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:TipoAvaliacao')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoAvaliacao entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('tipoavaliacao_edit', array('id' => $id)));
        }

        return $this->render('DefaultBundle:TipoAvaliacao:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a TipoAvaliacao entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('DefaultBundle:TipoAvaliacao')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TipoAvaliacao entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('tipoavaliacao'));
    }

    /**
     * Creates a form to delete a TipoAvaliacao entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tipoavaliacao_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
