<?php

namespace Plugged\DefaultBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Plugged\DefaultBundle\Entity\AtividadeMeta;
use Plugged\DefaultBundle\Form\AtividadeMetaType;

/**
 * AtividadeMeta controller.
 *
 */
class AtividadeMetaController extends Controller
{

    /**
     * Lists all AtividadeMeta entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('DefaultBundle:AtividadeMeta')->findAll();

        return $this->render('DefaultBundle:AtividadeMeta:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new AtividadeMeta entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new AtividadeMeta();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('atividademeta_show', array('id' => $entity->getMetaId()->getId())));
        }

        return $this->render('DefaultBundle:AtividadeMeta:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a AtividadeMeta entity.
     *
     * @param AtividadeMeta $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(AtividadeMeta $entity)
    {
        $form = $this->createForm(new AtividadeMetaType(), $entity, array(
            'action' => $this->generateUrl('atividademeta_create'),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * Displays a form to create a new AtividadeMeta entity.
     *
     */
    public function newAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new AtividadeMeta();
        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);
        $meta = $em->getRepository('DefaultBundle:Meta')->find($id);
        $entity->setMetaId($meta);
        $form   = $this->createCreateForm($entity);

        return $this->render('DefaultBundle:AtividadeMeta:new.html.twig', array(
            'entity' => $entity,
            'empresa' => $empresa,
            'meta' => $meta,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a AtividadeMeta entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);
        $meta = $em->getRepository('DefaultBundle:Meta')->find($id);

        $entities = $em->getRepository('DefaultBundle:AtividadeMeta')->findBy(array(
            'metaId' => $id
        ));

        return $this->render('DefaultBundle:AtividadeMeta:show.html.twig', array(
            'entities'      => $entities,
            'meta' => $meta,
            'empresa' => $empresa
            
        ));
    }

    /**
     * Displays a form to edit an existing AtividadeMeta entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:AtividadeMeta')->find($id);
        
        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);
        $meta = $em->getRepository('DefaultBundle:Meta')->find($entity->getMetaId());

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AtividadeMeta entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('DefaultBundle:AtividadeMeta:edit.html.twig', array(
            'entity'      => $entity,
            'empresa' => $empresa,
            'form'   => $editForm->createView(),
            'meta'   => $meta,
        ));
    }

    

    /**
    * Creates a form to edit a AtividadeMeta entity.
    *
    * @param AtividadeMeta $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(AtividadeMeta $entity)
    {
        $form = $this->createForm(new AtividadeMetaType(), $entity, array(
            'action' => $this->generateUrl('atividademeta_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        return $form;
    }
    /**
     * Edits an existing AtividadeMeta entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:AtividadeMeta')->find($id);
        $meta = $em->getRepository('DefaultBundle:Meta')->find($entity->getMetaId());
        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AtividadeMeta entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('atividademeta_show', array('id' => $entity->getMetaId()->getId())));
        }

        return $this->render('DefaultBundle:AtividadeMeta:edit.html.twig', array(
            'entity'      => $entity,
            'empresa' => $empresa,
            'form'   => $editForm->createView(),
            'meta' => $meta,
        ));
    }
    /**
     * Deletes a AtividadeMeta entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('DefaultBundle:AtividadeMeta')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find AtividadeMeta entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('atividademeta'));
    }

    /**
     * Creates a form to delete a AtividadeMeta entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('atividademeta_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
