<?php

namespace Plugged\DefaultBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Plugged\DefaultBundle\Entity\Resposta;
use Plugged\DefaultBundle\Entity\RespostaProfissional;

use Plugged\DefaultBundle\Form\RespostaType;

/**
 * Resposta controller.
 *
 */
class RespostaController extends Controller
{

    /**
     * Lists all Resposta entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('DefaultBundle:Resposta')->findAll();

        return $this->render('DefaultBundle:Resposta:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Resposta entity.
     *
     */
    public function createAction(Request $request,$id = null)
    {
        $em = $this->getDoctrine()->getManager();
        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);

        $entity = new Resposta();
        $pergunta = $this->getDoctrine()->getRepository("DefaultBundle:Quiz")->find($id);

        $entity->setPerguntaId($pergunta);

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();


            $profissionais = $this->getDoctrine()->getRepository("DefaultBundle:ProfissionalEmpresa")->findBy([
                "empresaId" => $empresa
            ]);

            foreach($profissionais as $prof){
                $params = array(
                    'id' => $prof->getProfissionalId()->getToken(),
                    'texto' => 'Olá tem um novo quiz!!!'
                );

                $this->get("firebase")->sendPush($params);

            }

            return $this->redirect($this->generateUrl('quiz_edit', array('id' => $entity->getPerguntaId()->getId())));
        }

        return $this->render('DefaultBundle:Resposta:new.html.twig', array(
            'entity' => $entity,
            "empresa" => $empresa,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Resposta entity.
     *
     * @param Resposta $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Resposta $entity)
    {
        $form = $this->createForm(new RespostaType(), $entity, array(
            'action' => $this->generateUrl('resposta_create',["id" => $entity->getPerguntaId()->getId()]),
            'method' => 'POST',
        ));



        return $form;
    }

    /**
     * Displays a form to create a new Resposta entity.
     *
     */
    public function newAction($id = null)
    {
        $em = $this->getDoctrine()->getManager();
        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);

        $pergunta = $this->getDoctrine()->getRepository("DefaultBundle:Quiz")->find($id);
        $entity = new Resposta();

        $entity->setPerguntaId($pergunta);

        $form   = $this->createCreateForm($entity);



        return $this->render('DefaultBundle:Resposta:new.html.twig', array(
            'entity' => $entity,
            "empresa" => $empresa,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Resposta entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:Resposta')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Resposta entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('DefaultBundle:Resposta:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Resposta entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:Resposta')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Resposta entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('DefaultBundle:Resposta:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Resposta entity.
    *
    * @param Resposta $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Resposta $entity)
    {
        $form = $this->createForm(new RespostaType(), $entity, array(
            'action' => $this->generateUrl('resposta_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Resposta entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:Resposta')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Resposta entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('resposta_edit', array('id' => $id)));
        }

        return $this->render('DefaultBundle:Resposta:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Resposta entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);

            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('DefaultBundle:Resposta')->find($id);

            $pergunta = $entity->getPerguntaId();

            $em->remove($entity);
            $em->flush();




        return $this->redirect($this->generateUrl('quiz_edit',array(
            "id" => $pergunta->getId()
        )));
    }

    /**
     * Creates a form to delete a Resposta entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('resposta_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    public function setRespostaAction(Request $request){

        $dados = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $resposta = $em->getRepository("DefaultBundle:Resposta")->find($dados["resposta"]);


        $quiz = $em->getRepository("DefaultBundle:Quiz")->find($resposta->getPerguntaId());
        $empresa = $em->getRepository("DefaultBundle:Empresa")->find($quiz->getEmpresaId());

        $profissional = $em->getRepository("DefaultBundle:Profissional")->find($dados["profissional"]);

        $respostaProfissional = new RespostaProfissional();

        $respostaProfissional->setRespostaId($resposta)
        ->setPerguntaId($quiz)
        ->setProfissionalId($profissional)
        ->setData(new \DateTime("now"))
        ->setStatus(1)
        ->setObservacao("")
        ->setEmpresaId($empresa);

        $em->persist($respostaProfissional);
        $em->flush();


        return new JsonResponse([
            "id" => "1",
            "pergunta" => "Obrigado por sua colaboração!",
            "respostas" => array(
                array(
                    "id" => "1",
                    "descricao" => "até a proxima!"
                ),


            )
        ]);
    }
}
