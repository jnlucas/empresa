<?php

namespace Plugged\DefaultBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Plugged\DefaultBundle\Entity\Ocupacao;
use Plugged\DefaultBundle\Form\OcupacaoType;

/**
 * Ocupacao controller.
 *
 */
class OcupacaoController extends Controller
{

    /**
     * Lists all Ocupacao entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('DefaultBundle:Ocupacao')->findAll();

        return $this->render('DefaultBundle:Ocupacao:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Ocupacao entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Ocupacao();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('ocupacao_show', array('id' => $entity->getId())));
        }

        return $this->render('DefaultBundle:Ocupacao:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Ocupacao entity.
     *
     * @param Ocupacao $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Ocupacao $entity)
    {
        $form = $this->createForm(new OcupacaoType(), $entity, array(
            'action' => $this->generateUrl('ocupacao_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Ocupacao entity.
     *
     */
    public function newAction()
    {
        $entity = new Ocupacao();
        $form   = $this->createCreateForm($entity);

        return $this->render('DefaultBundle:Ocupacao:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Ocupacao entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:Ocupacao')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Ocupacao entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('DefaultBundle:Ocupacao:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Ocupacao entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:Ocupacao')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Ocupacao entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('DefaultBundle:Ocupacao:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Ocupacao entity.
    *
    * @param Ocupacao $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Ocupacao $entity)
    {
        $form = $this->createForm(new OcupacaoType(), $entity, array(
            'action' => $this->generateUrl('ocupacao_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Ocupacao entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:Ocupacao')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Ocupacao entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('ocupacao_edit', array('id' => $id)));
        }

        return $this->render('DefaultBundle:Ocupacao:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Ocupacao entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('DefaultBundle:Ocupacao')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Ocupacao entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('ocupacao'));
    }

    /**
     * Creates a form to delete a Ocupacao entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('ocupacao_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
