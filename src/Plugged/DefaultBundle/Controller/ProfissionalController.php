<?php

namespace Plugged\DefaultBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Plugged\DefaultBundle\Entity\Profissional;
use Plugged\DefaultBundle\Form\ProfissionalType;


/**
 * Profissional controller.
 *
 */
class ProfissionalController extends Controller
{

    /**
     * Lists all Profissional entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);
        $entities = $em->getRepository('DefaultBundle:ProfissionalEmpresa')->findBy(array('empresaId' => $empresa));
        

        return $this->render('DefaultBundle:Profissional:index.html.twig', array(
            'profissionais' => $entities,
            'empresa' => $empresa,
        ));
    }
    /**
     * Creates a new Profissional entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Profissional();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('profissional_show', array('id' => $entity->getId())));
        }

        return $this->render('DefaultBundle:Profissional:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Profissional entity.
     *
     * @param Profissional $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Profissional $entity)
    {
        $form = $this->createForm(new ProfissionalType(), $entity, array(
            'action' => $this->generateUrl('profissional_create'),
            'method' => 'POST',
        ));

        
        return $form;
    }

    /**
     * Creates a form to create a Profissional entity.
     *
     * @param Profissional $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createShowForm(Profissional $entity,$id)
    {
        $form = $this->createForm(new ProfissionalType(), $entity, array(
            'action' => $this->generateUrl('profissional_show',['id' => $id]),
            'method' => 'POST',
        ));

        
        return $form;
    }

    /**
     * Displays a form to create a new Profissional entity.
     *
     */
    public function newAction()
    {
        $entity = new Profissional();
        $form   = $this->createCreateForm($entity);

        return $this->render('DefaultBundle:Profissional:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Profissional entity.
     *
     */
    public function showAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);

        $entity = $em->getRepository('DefaultBundle:Profissional')->find($id);

        $form = $this->createShowForm($entity,$id);
        
        if($request->getMethod()== 'POST'){
            
            $form->handleRequest($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                return $this->redirect($this->generateUrl('profissional'));
            }
            
        }
        

        return $this->render('DefaultBundle:Profissional:show.html.twig', array(
            'entity'      => $entity,
            'empresa' => $empresa,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Profissional entity.
     *
     */
    public function apiAction(Request $request)
    {
        $dados = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('DefaultBundle:Profissional')->findOneBy(['cpf'=>$dados['cpf']]);

        $equipe = $em->getRepository('DefaultBundle:EquipeProfissional')->findBy([
            'profissionalId'=>$entity,
            'status' => 1,
        ]);
        $equipes = [];
        foreach($equipe as $e){
            
            $equipes[] = array(
                'id' => $e->getEquipeId()->getId(),
                'descricao' => $e->getEquipeId()->getDescricao(),
                
            );
        }
        
        $atividade = $em->getRepository('DefaultBundle:AtividadeMetaProfissional')->findBy([
            'profissionalId'=>$entity,
            'status' => 1
        ]);
        
        $atividades = [];
        foreach($atividade as $a){
            $atividades[] = array(
                'id' => $a->getAtividadeMetaId()->getId(),
                'descricao' => $a->getAtividadeMetaId()->getDescricao(),
            );
        }

        $meta = '';
        if($atividade){
            $meta = $em->getRepository('DefaultBundle:Meta')->findOneBy(['id'=>$atividade[0]->getAtividadeMetaId()->getMetaId()]);
            $meta = $meta->getId();
        }

        $profissional = array(
            'nome' => ($entity->getNome()),
            'id' => ($entity->getId()),
            
            'email' => ($entity->getEmail()),
            'equipes' => $equipes,
            'atividades' => $atividades,
            'meta' => $meta, 
            'foto' => 'http://api.14mob.com/bundles/default/uploads/'.$entity->getFoto()
        );
        
        return new JsonResponse(($profissional));
    }

        /**
     * Finds and displays a Profissional entity.
     *
     */
    public function getApiAction(Request $request,$id)
    {
        header('Access-Control-Allow-Origin: *');  
        $dados = $request->request->all();
        
        $dados['cpf'] = $id;
        
        
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('DefaultBundle:Profissional')->findOneBy(['cpf'=>$dados['cpf']]);

        $equipe = $em->getRepository('DefaultBundle:EquipeProfissional')->findBy([
            'profissionalId'=>$entity,
            'status' => 1,
        ]);
        $equipes = [];
        foreach($equipe as $e){
            
            $equipes[] = array(
                'id' => $e->getEquipeId()->getId(),
                'descricao' => $e->getEquipeId()->getDescricao(),
                
            );
        }
        
        $atividade = $em->getRepository('DefaultBundle:AtividadeMetaProfissional')->findBy([
            'profissionalId'=>$entity,
            'status' => 1
        ]);
        
        $atividades = [];
        foreach($atividade as $a){
            $atividades[] = array(
                'id' => $a->getAtividadeMetaId()->getId(),
                'descricao' => $a->getAtividadeMetaId()->getDescricao(),
            );
        }

        $meta = '';
        if($atividade){
            $meta = $em->getRepository('DefaultBundle:Meta')->findOneBy(['id'=>$atividade[0]->getAtividadeMetaId()->getMetaId()]);
            $meta = $meta->getId();
        }

        $profissional = array(
            'nome' => ($entity->getNome()),
            'id' => ($entity->getId()),
            
            'email' => ($entity->getEmail()),
            'equipes' => $equipes,
            'atividades' => $atividades,
            'meta' => $meta, 
            'foto' => 'http://api.14mob.com/bundles/default/uploads/'.$entity->getFoto()
        );
        
        return new JsonResponse(($profissional));
    }


    /**
     * Finds and displays a Profissional entity.
     *
     */
    public function apiTokenAction(Request $request)
    {
        $dados = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('DefaultBundle:Profissional')->findOneBy(['cpf'=>$dados['cpf']]);
        
        if($dados['token']){
            $entity->setToken($dados['token']);
        }
        
        $em->flush();
        
        return new JsonResponse(['status' => true]);
    }



    /**
     * Displays a form to edit an existing Profissional entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:Profissional')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Profissional entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('DefaultBundle:Profissional:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Profissional entity.
    *
    * @param Profissional $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Profissional $entity)
    {
        $form = $this->createForm(new ProfissionalType(), $entity, array(
            'action' => $this->generateUrl('profissional_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        return $form;
    }
    /**
     * Edits an existing Profissional entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:Profissional')->find($id);

        if (!$entity) {
            return new JsonResponse([]);
        }

        $dados = $request->request->all();
        
        $entity->setFoto($dados['foto']);
        
        $em->flush();
       $equipe = $em->getRepository('DefaultBundle:EquipeProfissional')->findBy([
            'profissionalId'=>$entity,
            'status' => 1,
        ]);
        $equipes = [];
        foreach($equipe as $e){
            
            $equipes[] = array(
                'id' => $e->getEquipeId()->getId(),
                'descricao' => $e->getEquipeId()->getDescricao(),
                
            );
        }
        
        $atividade = $em->getRepository('DefaultBundle:AtividadeMetaProfissional')->findBy([
            'profissionalId'=>$entity,
            'status' => 1
        ]);
        
        $atividades = [];
        foreach($atividade as $a){
            $atividades[] = array(
                'id' => $a->getAtividadeMetaId()->getId(),
                'descricao' => $a->getAtividadeMetaId()->getDescricao(),
            );
        }

        $meta = '';
        if($atividade){
            $meta = $em->getRepository('DefaultBundle:Meta')->findOneBy(['id'=>$atividade[0]->getAtividadeMetaId()->getMetaId()]);
            $meta = $meta->getId();
        }
        
        $profissional = array(
            'nome' => ($entity->getNome()),
            'id' => ($entity->getId()),
            
            'email' => ($entity->getEmail()),
            'equipes' => $equipes,
            'atividades' => $atividades,
            'meta' => $meta, 
            'foto' => 'http://api.14mob.com/bundles/default/uploads/'.$entity->getFoto()
        );
        return new JsonResponse($profissional);
    }
    /**
     * Deletes a Profissional entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('DefaultBundle:Profissional')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Profissional entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('profissional'));
    }
    
    /**
     * Deletes a Profissional entity.
     *
     */
    public function avaliacoesAction(Request $request, $id)
    {
        
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('DefaultBundle:Profissional')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Profissional entity.');
        }

           

        return $this->render('DefaultBundle:Profissional:avaliacoes.html.twig', array(
            'entity'      => $entity,
            
        ));
    }

    /**
     * Creates a form to delete a Profissional entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('profissional_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
