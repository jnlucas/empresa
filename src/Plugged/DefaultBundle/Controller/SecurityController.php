<?php

namespace Plugged\DefaultBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Session\Session;
use Plugged\DefaultBundle\Form\LoginType;

class SecurityController extends Controller
{
    public function loginAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        $form = $this->createCreateForm();

        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->render('DefaultBundle:Security:login.html.twig', array(
                // last username entered by the user
                'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                'error' => $error,
                'form' => $form->createView(),
        ));
    }

    public function securityCheckAction()
    {
        // The security layer will intercept this request
    }

    public function logoutAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        session_destroy();
        $session = new Session();

        return $this->redirect($this->generateUrl("login"));
    }

    /**
     *
     */
    private function createCreateForm()
    {
        $form = $this->createForm(new LoginType(), null, array(
            'action' => $this->generateUrl('login_check'),
            'method' => 'POST',
            'attr' => array(
                'class' => 'validar-formulario',
            ),
        ));
        $form->add("submit", "submit");

        return $form;
    }
}
