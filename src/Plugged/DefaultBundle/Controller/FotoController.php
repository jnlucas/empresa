<?php

namespace Plugged\DefaultBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Plugged\DefaultBundle\Entity\Foto;
use Plugged\DefaultBundle\Form\FotoType;

use Plugged\DefaultBundle\Entity\Anuncio;
use Plugged\DefaultBundle\Form\AnuncioType;


/**
 * Foto controller.
 *
 */
class FotoController extends Controller
{
	
	public function fotosAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $anuncio = $em->getRepository('DefaultBundle:Anuncio')->find($id);
		
		$entity = new Foto();
        $form = $this->createCreateForm($entity,$id);
		
        
        return $this->render('DefaultBundle:Anuncio:fotos.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            'anuncio' => $anuncio
        ));
    }

    /**
     * Lists all Foto entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('DefaultBundle:Foto')->findAll();

        return $this->render('DefaultBundle:Foto:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Foto entity.
     *
     */
    public function createAction(Request $request,$id)
    {
    	$em = $this->getDoctrine()->getManager();
        $entity = new Foto();
        $form = $this->createCreateForm($entity,$id);
		$anuncio = $em->getRepository('DefaultBundle:Anuncio')->find($id);
		$entity->setAnuncioId($anuncio);
		$form->handleRequest($request);

        if ($form->isValid()) {
            
        	
        	$UploadServices = $this->get('upload');
			$diretorio = $this->get('kernel')->getRootDir()."/../src/Plugged/DefaultBundle/Resources/public/images/";
			$entity->setFoto($UploadServices->Envia_Arquivo($_FILES['plugged_defaultbundle_foto'],time(),500,500,$diretorio,1));
			$entity->setData(new \DateTime('now'));
                
            
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('painel_anuncio_fotos', array('id' => $id)));
        }

        return $this->render('DefaultBundle:Foto:fotos.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'anuncio' => $anuncio
        ));
    }

    /**
     * Creates a form to create a Foto entity.
     *
     * @param Foto $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Foto $entity,$id = null)
    {
        $form = $this->createForm(new FotoType(), $entity, array(
            'action' => $this->generateUrl('painel_foto_create',array('id' => $id)),
            'method' => 'POST',
            'attr' => array(
				'class' => 'form-horizontal'
			),
        ));
        
        return $form;
    }

    /**
     * Displays a form to create a new Foto entity.
     *
     */
    public function newAction()
    {
        $entity = new Foto();
        $form   = $this->createCreateForm($entity);

        return $this->render('DefaultBundle:Foto:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Foto entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:Foto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Foto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('DefaultBundle:Foto:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Foto entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:Foto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Foto entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('DefaultBundle:Foto:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Foto entity.
    *
    * @param Foto $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Foto $entity)
    {
        $form = $this->createForm(new FotoType(), $entity, array(
            'action' => $this->generateUrl('painel_foto_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Foto entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:Foto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Foto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('painel_foto_edit', array('id' => $id)));
        }

        return $this->render('DefaultBundle:Foto:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Foto entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('DefaultBundle:Foto')->find($id);
        $em->remove($entity);
        $em->flush();
        return $this->redirect($this->generateUrl('painel_anuncio_fotos', array('id' => $entity->getAnuncioId()->getId())));
    }

    /**
     * Creates a form to delete a Foto entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('painel_foto_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
