<?php

namespace Plugged\DefaultBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Plugged\DefaultBundle\Entity\Quiz;
use Plugged\DefaultBundle\Form\QuizType;

/**
 * Quiz controller.
 *
 */
class QuizController extends Controller
{

    /**
     * Lists all Quiz entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);
        $entities = $em->getRepository('DefaultBundle:Quiz')->findBy(array('empresaId' => $empresa));


        return $this->render('DefaultBundle:Quiz:index.html.twig', array(
            'entities' => $entities,
            'empresa' => $empresa,
        ));
    }
    /**
     * Creates a new Quiz entity.
     *
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);
        $entity = new Quiz();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em->persist($entity);
            $em->flush();


            $profissionais = $this->getDoctrine()->getRepository("DefaultBundle:ProfissionalEmpresa")->findBy([
                "empresaId" => $empresa
            ]);

            foreach($profissionais as $prof){
                $params = array(
                    'id' => $prof->getProfissionalId()->getToken(),
                    'texto' => 'Olá tem um novo quiz!!!'
                );

                print_r($params);

                $this->get("firebase")->sendPush($params);

            }

            return $this->redirect($this->generateUrl('quiz_edit', array('id' => $entity->getId())));
        }

        return $this->render('DefaultBundle:Quiz:new.html.twig', array(
            'entity' => $entity,
            "empresa" => $empresa,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Quiz entity.
     *
     * @param Quiz $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Quiz $entity)
    {
        $form = $this->createForm(new QuizType(), $entity, array(
            'action' => $this->generateUrl('quiz_create'),
            'method' => 'POST',
        ));



        return $form;
    }

    /**
     * Displays a form to create a new Quiz entity.
     *
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();
        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);
        $entity = new Quiz();
        $form   = $this->createCreateForm($entity);

        return $this->render('DefaultBundle:Quiz:new.html.twig', array(
            'entity' => $entity,
            "empresa" => $empresa,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Quiz entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:Quiz')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Quiz entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('DefaultBundle:Quiz:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Quiz entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);


        $entity = $em->getRepository('DefaultBundle:Quiz')->find($id);

        $respostas = $em->getRepository('DefaultBundle:Resposta')->findBy(["perguntaId" => $id]);
        $respostaProfissional = $em->getRepository('DefaultBundle:RespostaProfissional')->findBy(["perguntaId" => $id]);

        foreach ($respostas as $resp){

            foreach($respostaProfissional as $prof){

                if($prof->getRespostaId()->getId() == $resp->getId()){
                    $resp->setQuantidade($resp->getQuantidade()+1);
                }
            }
        }



        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Quiz entity.');
        }

        $form = $this->createEditForm($entity);


        return $this->render('DefaultBundle:Quiz:edit.html.twig', array(
            'entity' => $entity,
            "empresa" => $empresa,
            "respostas" => $respostas,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to edit a Quiz entity.
    *
    * @param Quiz $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Quiz $entity)
    {
        $form = $this->createForm(new QuizType(QuizType::EDIT), $entity, array(
            'action' => $this->generateUrl('quiz_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));



        return $form;
    }
    /**
     * Edits an existing Quiz entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);


        $entity = $em->getRepository('DefaultBundle:Quiz')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Quiz entity.');
        }

        $form = $this->createEditForm($entity);

        $respostas = $em->getRepository('DefaultBundle:Resposta')->findBy(["perguntaId" => $id]);

        $profissionais = $this->getDoctrine()->getRepository("DefaultBundle:ProfissionalEmpresa")->findBy([
            "empresaId" => $empresa
        ]);

        foreach($profissionais as $prof){
            $params = array(
                'id' => $prof->getProfissionalId()->getToken(),
                'texto' => 'Olá tem um novo quiz!!!'
            );

            $this->get("firebase")->sendPush($params);

        }

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('quiz_edit', array('id' => $id)));
        }

        return $this->render('DefaultBundle:Quiz:edit.html.twig', array(
            'entity' => $entity,
            "empresa" => $empresa,
            "respostas" => $respostas,
            'form'   => $form->createView(),

        ));
    }
    /**
     * Deletes a Quiz entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('DefaultBundle:Quiz')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Quiz entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('quiz'));
    }

    /**
     * Creates a form to delete a Quiz entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('quiz_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    public function listAction(Request $request){

        $dados = $request->request->all();

        $quiz = $this->getDoctrine()->getRepository("DefaultBundle:Quiz")->buscarQuiz($dados);
        $retorno = [];
        if($quiz){
            foreach($quiz->getRespostas() as $resposta)
            {
                $respostas[] = array(
                    "id" => $resposta->getId(),
                    "descricao" => $resposta->getResposta()
                );

            }
            $retorno = array(
                "id" => $quiz->getId(),
                "pergunta" => $quiz->getPergunta(),
                "respostas" => $respostas
            );

        }else{
            $retorno = array(
                "id" => "1",
                "pergunta" => "tudo okay por aqui!",
                "respostas" => array(

                    array(
                        "id" => "99",
                        "descricao" => "em breve teremos um novo quiz!"
                    ),

                )
            );
        }


        return new JsonResponse($retorno);
    }
}
