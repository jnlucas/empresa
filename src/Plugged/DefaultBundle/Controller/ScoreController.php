<?php

namespace Plugged\DefaultBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ScoreController extends Controller
{
    public function apiAction(Request $request,$profissionalId,$metaId)
    {

        $pontosProfissionais = $this->get("score")->getScore($request,$profissionalId,$metaId);


        return new JsonResponse($pontosProfissionais);
    }

    public function apiAvaliacaoAction(Request $request,$profissionalId,$metaId)
    {

        $meta = $this->getDoctrine()->getRepository('DefaultBundle:Meta')->find($metaId);

        $avaliacoesProfissional = $this->getDoctrine()->getRepository('DefaultBundle:ProfissionalAvaliacao')->findBy(['profissionalId' => $profissionalId,'metaId' => $metaId]);
        
        $pontosProfissionais = [];
        $pontos = 0;
        foreach($avaliacoesProfissional as $avaliacao)
        {
            $pontos += $avaliacao->getPontuacao();
            $pontosProfissionais[] = array(
                'superiorImediato' => $avaliacao->getSuperiorImediato(),
                'pontos' => $avaliacao->getPontuacao(),
                'observacao' => $avaliacao->getObservacao(),
                'data' => $avaliacao->getData()->format('d-m-Y'),
                'meta' => $meta->getPontos(),
                'tipoAvaliacao' => $avaliacao->getTipoAvaliacaoId()->getNome(),
                'foto' => 'http://14mob.com/img/LOGO.png'
            );
        }

        return new JsonResponse($pontosProfissionais);
    }

}
