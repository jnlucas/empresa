<?php

namespace Plugged\DefaultBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Plugged\DefaultBundle\Entity\Equipe;
use Plugged\DefaultBundle\Form\EquipeType;

use Plugged\DefaultBundle\Entity\EquipeProfissional;
use Plugged\DefaultBundle\Form\EquipeProfissionalType;


/**
 * Equipe controller.
 *
 */
class EquipeController extends Controller
{

    /**
     * Lists all Equipe entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $id = 1;
        $entity = $em->getRepository('DefaultBundle:Empresa')->find($id);
        $equipes = $em->getRepository('DefaultBundle:Equipe')->findBy(array(
            'empresaId' => $entity
        ));

        return $this->render('DefaultBundle:Equipe:index.html.twig', array(
            'entity' => $entity,
            'equipes' => $equipes,
        ));
    }
    /**
     * Creates a new Equipe entity.
     *
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);
        
        $entity = new Equipe();

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('equipe'));
        }

        return $this->render('DefaultBundle:Equipe:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'empresa'      => $empresa,
            'profissionais'      => [],
        ));
    }

    /**
     * Creates a form to create a Equipe entity.
     *
     * @param Equipe $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Equipe $entity)
    {
        $form = $this->createForm(new EquipeType(), $entity, array(
            'action' => $this->generateUrl('equipe_create'),
            'method' => 'POST',
        ));

        
        return $form;
    }

    /**
     * Displays a form to create a new Equipe entity.
     *
     */
    public function newAction()
    {

        $em = $this->getDoctrine()->getManager();
        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);
        $equipe = new Equipe();
        $profissionais  = [];
        $form = $this->createCreateForm($equipe);

        return $this->render('DefaultBundle:Equipe:new.html.twig', array(
            'equipe'      => $equipe,
            'empresa'      => $empresa,
            'profissionais'      => $profissionais,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Equipe entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:Equipe')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Equipe entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('DefaultBundle:Equipe:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Equipe entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);
        $equipe = $em->getRepository('DefaultBundle:Equipe')->find($id);
        $profissionais = $em->getRepository('DefaultBundle:EquipeProfissional')->findBy(array(
            'equipeId' => $equipe->getId()
        ));
        
        

        $form = $this->createEditForm($equipe);
        

        return $this->render('DefaultBundle:Equipe:edit.html.twig', array(
            'equipe'      => $equipe,
            'empresa'      => $empresa,
            'profissionais'      => $profissionais,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to edit a Equipe entity.
    *
    * @param Equipe $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Equipe $entity)
    {
        $form = $this->createForm(new EquipeType(), $entity, array(
            'action' => $this->generateUrl('equipe_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'attr' => array(
                'class' => 'formulario'
            )
        ));

        return $form;
    }

    /**
    * Creates a form to edit a Equipe entity.
    *
    * @param Equipe $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditProfissionalForm(EquipeProfissional $entity, $equipe)
    {
        $form = $this->createForm(new EquipeProfissionalType(), $entity, array(
            'action' => $this->generateUrl('equipe_update', array('id' => $equipe->getId())),
            'method' => 'PUT',
            'attr' => array(
                'class' => 'formulario'
            )
        ));

        return $form;
    }
    /**
     * Edits an existing Equipe entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);
        $equipe = $em->getRepository('DefaultBundle:Equipe')->find($id);
        $profissionais = $em->getRepository('DefaultBundle:EquipeProfissional')->findBy(array(
            'equipeId' => $equipe->getId()
        ));

        $form = $this->createEditForm($equipe);
        $form->handleRequest($request);

        if ($form->isValid()) {
            
            $equipeProfissional = new EquipeProfissional();
            
            $equipeProfissional->setEmpresaId($empresa)
            ->setEquipeId($equipe)
            ->setProfissionalId($form->getData()->getProfissionalId()->getProfissionalId());

            try{
                $em->persist($equipeProfissional);
                $em->flush();
                 $request->getSession()->getFlashBag()->add('success', "Equipe Atualizada com sucesso!");
                 return $this->redirect($this->generateUrl("equipe_edit",array('id' => $equipe->getId())));
            }catch(\Exception $e){
                $request->getSession()->getFlashBag()->add('danger', "Erro ao atualizar equipe!");
            }

        }
        $request->getSession()->getFlashBag()->add('danger', "Erro ao atualizar equipe!");

        return $this->render('DefaultBundle:Equipe:edit.html.twig', array(
            'empresa'      => $empresa,
            'equipe'      => $equipe,
            'profissionais'      => $profissionais,
            'form'   => $form->createView(),
        ));
    }
    /**
     * Deletes a Equipe entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('DefaultBundle:Equipe')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Equipe entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('equipe'));
    }

    /**
     * Creates a form to delete a Equipe entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('equipe_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
