<?php

namespace Plugged\DefaultBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
       $em = $this->getDoctrine()->getManager();
       $dados = $request->query->all();

       $profissionais = [];
       if(isset($dados["ads"])){
           $profissionais = $em->getRepository("DefaultBundle:ProfissionalEmpresa")->getProfissionais($dados);

       }


        return $this->render('DefaultBundle:Default:index.html.twig', array(
            "profissionais" => $profissionais
	    ));
    }

	/**
	 *
	 */
	public function homeAction()
    {
    	//return $this->redirect($this->generateUrl('empresa'));

        return $this->render('DefaultBundle:Default:home.html.twig', array(

	    ));
    }

    public function profileAction(Request $request, $nickname)
      {

          $profissional = $this->getDoctrine()->getRepository("DefaultBundle:Profissional")->findOneBy(["nickname" => $nickname]);
          $profissionalEmpresa = $this->getDoctrine()->getRepository("DefaultBundle:ProfissionalEmpresa")->findOneBy(["profissionalId" => $profissional]);

          $profissionalAvaliacao = $this->getDoctrine()->getRepository("DefaultBundle:ProfissionalAvaliacao")
            ->findBy(["profissionalId" => $profissional->getId()],["id" => "desc"]);

          $profissionalAtividade = $this->getDoctrine()->getRepository("DefaultBundle:AtividadeMetaProfissional")
              ->findBy(["profissionalId" => $profissional->getId()]);
         $metas = [];
         foreach($profissionalAtividade as $atividade){
             $metas[$atividade->getAtividadeMetaId()->getMetaId()->getId()] = $atividade->getAtividadeMetaId()->getMetaId();
         }

          return $this->render('DefaultBundle:Default:profile.html.twig', array(
              "profissional" => $profissional,
              "profissionalEmpresa" => $profissionalEmpresa,
              "profissionalAvaliacao" => isset($profissionalAvaliacao[0])?$profissionalAvaliacao[0]:null,
              "avaliacoes" => $profissionalAvaliacao,
              "profissionalAtividade"=> $profissionalAtividade,
              "metas" => $metas


          ));
      }



	/**
	 *
	 */
	public function sendMessageAction(Request $request, $id)
    {
    	$dados = $request->request->all();
		$entity = $this->getDoctrine()->getRepository('DefaultBundle:Anuncio')->find($id);

		 $message = \Swift_Message::newInstance()
        ->setSubject('Contato Anuncio: '.$entity->getId())
        ->setFrom('site@youplugged.com.br')
        ->setTo($entity->getEmail())
		//->setPort(587)
		//->setEncryption()
        ->setBody(
            $this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                'DefaultBundle:Default:email.html.twig',
                array('entity' => $entity,'dados' => $dados)
            ),
            'text/html'
        );
		try{
			$this->get('mailer')->send($message);
		}catch(\Exception $e){
			dump($e);
		}



        return $this->render('DefaultBundle:Default:detalhe.html.twig', array(
       		'entity' => $entity,
	    ));
    }
}
