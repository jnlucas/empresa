<?php

namespace Plugged\DefaultBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Plugged\DefaultBundle\Entity\ProfissionalAvaliacao;
use Plugged\DefaultBundle\Form\ProfissionalAvaliacaoType;

/**
 * ProfissionalAvaliacao controller.
 *
 */
class ProfissionalAvaliacaoController extends Controller
{

    /**
     * Lists all ProfissionalAvaliacao entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $id = 1;
        $empresa = $em->getRepository('DefaultBundle:Empresa')->find($id);
        
        
        $entities = $em->getRepository('DefaultBundle:ProfissionalAvaliacao')->findBy(array(
            'empresaId' => $empresa
        ));
        $avaliacao = [];
        foreach($entities as $entity){
            
            $avaliacao[$entity->getMetaId()->getId()]['meta'] = $entity;
            $avaliacao[$entity->getMetaId()->getId()]['avaliacao'][] = $entity;
            
        }
        
        return $this->render('DefaultBundle:ProfissionalAvaliacao:index.html.twig', array(
            'entities' => $entities,
            'empresa' => $empresa,
            'avaliacoes' => $avaliacao
        ));
    }
    /**
     * Creates a new ProfissionalAvaliacao entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new ProfissionalAvaliacao();
        $em = $this->getDoctrine()->getManager();
        $id = 1;
        $empresa = $em->getRepository('DefaultBundle:Empresa')->find($id);
        
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entity->setData(new \DateTime('now'));
            $entity->setIsPublica(false);
            $em->persist($entity);
            if($entity->getProfissionalId()->getToken() != ''){
                $params = array(
                    'id' => $entity->getProfissionalId()->getToken(),
                    'texto' => 'Olá você acaba de ser avaliado!!!'
                );
                $this->get("firebase")->sendPush($params);
            }
            $em->flush();

            return $this->redirect($this->generateUrl('profissionalavaliacao_show',['id' => $entity->getProfissionalId()->getId()]));
        }

        return $this->render('DefaultBundle:ProfissionalAvaliacao:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'empresa' => $empresa,
        ));
    }

    /**
     * Creates a form to create a ProfissionalAvaliacao entity.
     *
     * @param ProfissionalAvaliacao $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ProfissionalAvaliacao $entity)
    {
        $form = $this->createForm(new ProfissionalAvaliacaoType(), $entity, array(
            'action' => $this->generateUrl('profissionalavaliacao_create'),
            'method' => 'POST',
        ));

       
        return $form;
    }

    /**
     * Displays a form to create a new ProfissionalAvaliacao entity.
     *
     */
    public function newAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);
        $profissional = $em->getRepository('DefaultBundle:Profissional')->find($id);
        $entity = new ProfissionalAvaliacao();
        
        $entity->setProfissionalId($profissional);
        
        $form   = $this->createCreateForm($entity);

        return $this->render('DefaultBundle:ProfissionalAvaliacao:new.html.twig', array(
            'entity' => $entity,
            'profissional' => $profissional,
            
            'form'   => $form->createView(),
            'empresa' => $empresa,
        ));
    }

    /**
     * Finds and displays a ProfissionalAvaliacao entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $empresa = $em->getRepository('DefaultBundle:Empresa')->find(1);
        $profissional = $em->getRepository('DefaultBundle:Profissional')->find($id);
        $entities = $em->getRepository('DefaultBundle:ProfissionalAvaliacao')->findBy(array(
            'empresaId' => $empresa,
            'profissionalId' => $id
        ));
        $avaliacao = [];
        foreach($entities as $entity){
            $avaliacao[$entity->getMetaId()->getId()]['meta'] = $entity;
            $avaliacao[$entity->getMetaId()->getId()]['avaliacao'][] = $entity;
            
        }

        return $this->render('DefaultBundle:ProfissionalAvaliacao:show.html.twig', array(
            'entities' => $entities,
            'empresa' => $empresa,
            'profissional' => $profissional,

            'avaliacoes' => $avaliacao
        ));
    }

    /**
     * Displays a form to edit an existing ProfissionalAvaliacao entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:ProfissionalAvaliacao')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProfissionalAvaliacao entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('DefaultBundle:ProfissionalAvaliacao:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a ProfissionalAvaliacao entity.
    *
    * @param ProfissionalAvaliacao $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(ProfissionalAvaliacao $entity)
    {
        $form = $this->createForm(new ProfissionalAvaliacaoType(), $entity, array(
            'action' => $this->generateUrl('profissionalavaliacao_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing ProfissionalAvaliacao entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DefaultBundle:ProfissionalAvaliacao')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProfissionalAvaliacao entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('profissionalavaliacao_edit', array('id' => $id)));
        }

        return $this->render('DefaultBundle:ProfissionalAvaliacao:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a ProfissionalAvaliacao entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('DefaultBundle:ProfissionalAvaliacao')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ProfissionalAvaliacao entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('profissionalavaliacao'));
    }

    /**
     * Creates a form to delete a ProfissionalAvaliacao entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('profissionalavaliacao_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
