<?php

namespace Plugged\DefaultBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AtividadeMetaProfissionalType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('profissionalId', 'entity', array(
                'class' => 'DefaultBundle:Profissional',
                'property' => 'nome',
                'label' => 'Profissional',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('atividadeMetaId', 'entity', array(
                'class' => 'DefaultBundle:AtividadeMeta',
                'property' => 'descricao',
                'label' => 'Atividade',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            
            ->add('status','choice',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'choices' => array(
                    '1' => 'ativo',
                    '0' => 'bloqueado',
                ),
                'label' => 'Status',
                'required' => true
                
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Plugged\DefaultBundle\Entity\AtividadeMetaProfissional'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'plugged_defaultbundle_atividademetaprofissional';
    }
}
