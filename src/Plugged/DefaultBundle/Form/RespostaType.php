<?php

namespace Plugged\DefaultBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RespostaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('resposta',"text", array(
                "label" => "Resposta",
                "attr" => array(
                    "class" => "form-control"
                )
            ))
            ->add('status','choice',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'choices' => array(
                    '1' => 'ativo',
                    '0' => 'bloqueado',
                ),
                'label' => 'Status',
                'required' => true
                
            ))
            
            ->add('perguntaId', 'entity', array(
                'class' => 'DefaultBundle:Quiz',
                'property' => 'pergunta',
                'label' => 'Pergunta',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Plugged\DefaultBundle\Entity\Resposta'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'plugged_defaultbundle_resposta';
    }
}
