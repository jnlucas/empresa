<?php

namespace Plugged\DefaultBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProfissionalEmpresaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('profissionalId',new ProfissionalType())
            
            ->add('empresaId','entity',array(
                'class' => 'DefaultBundle:Empresa',
                'property' => 'nome',
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'Empresa'
            ))
            ->add('ocupacaoId','entity',array(
                'class' => 'DefaultBundle:Ocupacao',
                'property' => 'nome',
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'Ocupação'
            ))

            ->add('tempoTrabalhado','text',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'Tempo de Casa'
            ))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Plugged\DefaultBundle\Entity\ProfissionalEmpresa'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'plugged_defaultbundle_profissionalempresa';
    }
}
