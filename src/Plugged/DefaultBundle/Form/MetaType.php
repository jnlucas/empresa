<?php

namespace Plugged\DefaultBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MetaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('descricao','text',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'Descrição',
                'required' => true
                
            ))
            ->add('pontos','text',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'Pontos',
                'required' => true
                
            ))
            ->add('empresaId', 'entity', array(
                'class' => 'DefaultBundle:Empresa',
                'property' => 'nome',
                'label' => 'Empresa',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('periodo', 'datetime', [
                'attr' => [
                    'class' => 'form-control date',
                    'data-rule-required' => false,
                    'data-date-format'=>"dd/mm/yyyy"
                ],
                'label' => 'Início',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'required' => true,
            ])
            ->add('periodoFim', 'datetime', [
                'attr' => [
                    'class' => 'form-control date',
                    'data-rule-required' => false,
                    'data-date-format'=>"dd/mm/yyyy"
                ],
                'label' => 'Fim',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'required' => true,
            ])
            ->add('status','choice',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'choices' => array(
                    '1' => 'ativo',
                    '0' => 'bloqueado',
                ),
                'label' => 'Status',
                'required' => true
                
            ))
            
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Plugged\DefaultBundle\Entity\Meta'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'plugged_defaultbundle_meta';
    }
}
