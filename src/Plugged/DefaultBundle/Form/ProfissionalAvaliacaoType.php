<?php

namespace Plugged\DefaultBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProfissionalAvaliacaoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('usuarioId','entity',array(
                'class' => 'DefaultBundle:Usuario',
                'property' => 'nome',
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'Usuario'
            ))
            ->add('metaId', 'entity', array(
                'class' => 'DefaultBundle:Meta',
                'property' => 'descricao',
                'label' => 'Meta',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            
            ->add('profissionalId','entity',array(
                'class' => 'DefaultBundle:Profissional',
                'property' => 'nome',
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'Profissional'
            ))
            
            ->add('empresaId','entity',array(
                'class' => 'DefaultBundle:Empresa',
                'property' => 'nome',
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'Empresa'
            ))
            ->add('tipoAvaliacaoId','entity',array(
                'class' => 'DefaultBundle:TipoAvaliacao',
                'property' => 'nome',
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'Tipo Avaliação'
            ))
            ->add('pontuacao','text',array(
                'label' => 'Pontos',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('superiorImediato','text',array(
                'label' => 'Avaliador',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('observacao','textarea',array(
                'label' => 'Observação',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('status','choice',array(
                'label' => 'Status',
                'choices' => array(
                    '1' => 'Ativo',
                    '0' => 'Inativo'
                ),
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Plugged\DefaultBundle\Entity\ProfissionalAvaliacao'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'plugged_defaultbundle_profissionalavaliacao';
    }
}
