<?php

namespace Plugged\DefaultBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class QuizType extends AbstractType
{
    const CREATE = "CREATE";
    const EDIT = "EDIT";

    private $type;

    public function __construct($type = QuizType::CREATE){

        $this->type = $type;
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {



        switch($this->type){

            case QuizType::CREATE:

                $this->createForm( $builder,  $options);
            break;

            case QuizType::EDIT:

                $this->editForm( $builder,  $options);
            break;
        }
        
    }


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function createForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pergunta',"text", array(
                "label" => "Pergunta",
                "attr" => array(
                    "class" => "form-control"
                )
            ))
            ->add('status','choice',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'choices' => array(
                    '1' => 'ativo',
                    '0' => 'bloqueado',
                ),
                'label' => 'Status',
                'required' => true
                
            ))
            
            ->add('empresaId', 'entity', array(
                'class' => 'DefaultBundle:Empresa',
                'property' => 'nome',
                'label' => 'Empresa',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))

            
        ;
    }


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function editForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pergunta',"text", array(
                "label" => "Pergunta",
                "attr" => array(
                    "class" => "form-control"
                )
            ))
            ->add('status','choice',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'choices' => array(
                    '1' => 'ativo',
                    '0' => 'bloqueado',
                ),
                'label' => 'Status',
                'required' => true
                
            ))
            
            ->add('empresaId', 'entity', array(
                'class' => 'DefaultBundle:Empresa',
                'property' => 'nome',
                'label' => 'Empresa',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))

            

            
        ;
    }

    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Plugged\DefaultBundle\Entity\Quiz'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'plugged_defaultbundle_quiz';
    }
}
