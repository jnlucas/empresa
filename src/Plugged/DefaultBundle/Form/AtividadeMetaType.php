<?php

namespace Plugged\DefaultBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AtividadeMetaType extends AbstractType
{
    private $type;

    public function __construct($type = "create"){

        $this->type = $type;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        switch ($this->type) {
            case 'create':
                $this->create($builder,$options);

            break;

            case 'import':
                $this->import($builder,$options);
            break;

            default:
                // code...
                break;
        }
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function create(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('descricao','text',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'Descrição',
                'required' => true

            ))
            ->add('protocolo','text',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'Protocolo',
                'required' => true

            ))
            ->add('pontos','text',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'Pontos',
                'required' => true

            ))
            ->add('vencimento', 'datetime', [
                'attr' => [
                    'class' => 'form-control date',
                    'data-rule-required' => false,
                    'data-date-format'=>"dd/mm/yyyy"
                ],
                'label' => 'Vencimento',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'required' => true,
            ])
            ->add('metaId', 'entity', array(
                'class' => 'DefaultBundle:Meta',
                'property' => 'descricao',
                'label' => 'Meta',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('status','choice',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'choices' => array(
                    '1' => 'ativo',
                    '0' => 'bloqueado',
                    '2' => 'Entregue'
                ),
                'label' => 'Status',
                'required' => true

            ))
        ;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function import(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('descricao','text',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'Descrição',
                'required' => true

            ))
            ->add('protocolo','text',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'Protocolo',
                'required' => true

            ))
            ->add('pontos','text',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'Pontos',
                'required' => true

            ))
            ->add('vencimento', 'datetime', [
                'attr' => [
                    'class' => 'form-control date',
                    'data-rule-required' => false,
                    'data-date-format'=>"dd/mm/yyyy"
                ],
                'label' => 'Vencimento',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'required' => true,
            ])
            ->add('metaId', 'entity', array(
                'class' => 'DefaultBundle:Meta',
                'property' => 'descricao',
                'label' => 'Meta',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('status','choice',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'choices' => array(
                    '1' => 'ativo',
                    '0' => 'bloqueado',
                    '2' => 'Entregue'
                ),
                'label' => 'Status',
                'required' => true

            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Plugged\DefaultBundle\Entity\AtividadeMeta'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'plugged_defaultbundle_atividademeta';
    }
}
