<?php

namespace Plugged\DefaultBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UsuarioType extends AbstractType
{
	public $param;
	
	public function __construct($param){
		$this->param = $param;
	}
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nome','text',array(
				'label' => 'Nome',
				'attr' => array(
					'class' => 'form-control'
				)
			))
            ->add('email','text',array(
				'label' => 'Email',
				'attr' => array(
					'class' => 'form-control',
					'data-rule-email' => true
				)
			))
            ->add('telefone','text',array(
				'label' => 'Telefone',
				'attr' => array(
					'class' => 'form-control'
				)
			))
            
            ->add('isEmpresa','choice',array(
				'label' => 'É empresa',
				'attr' => array(
					'class' => 'form-control'
				),
				'empty_value' => 'Selecione',
				'choices' => array(
					'1' => 'Sim',
					'0' => 'Não'
				),
			))
            ->add('nomeEmpresa','text',array(
				'label' => 'Nome Empresa',
				'attr' => array(
					'class' => 'form-control'
				)
			))
            ->add('endereco','text',array(
				'label' => 'Endereço',
				'attr' => array(
					'class' => 'form-control',
					'readonly' => 'readonly'
				)
			))
            ->add('numero','text',array(
				'label' => 'Numero',
				'attr' => array(
					'class' => 'form-control'
				)
			))
            ->add('cidade','text',array(
				'label' => 'Cidade',
				'attr' => array(
					'class' => 'form-control',
					'readonly' => 'readonly'
				)
			))
            ->add('estado','text',array(
				'label' => 'Estado',
				'attr' => array(
					'class' => 'form-control',
					'readonly' => 'readonly'
				)
			))
            ->add('cep','text',array(
				'label' => 'Cep',
				'attr' => array(
					'class' => 'form-control cep'
				)
			))
        ;
		
		if($this->param == 'new'){
			$builder
			->add('username','text',array(
				'label' => 'Usuário',
				'attr' => array(
					'class' => 'form-control'
				)
			))
			->add('password','password',array(
				'label' => 'Senha',
				'attr' => array(
					'class' => 'form-control'
				)
			));
		}
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Plugged\DefaultBundle\Entity\Usuario'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'plugged_defaultbundle_usuario';
    }
}
