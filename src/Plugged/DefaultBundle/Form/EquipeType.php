<?php

namespace Plugged\DefaultBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EquipeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nome','text',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'Nome',
                'required' => true
                
            ))
            ->add('descricao','text',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'Descrição',
                'required' => true
                
            ))
            ->add('empresaId', 'entity', array(
                'class' => 'DefaultBundle:Empresa',
                'property' => 'nome',
                'label' => 'Empresa',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('responsavel','text',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'Responsavel',
                'required' => true
                
            ))
            ->add('profissionalId', new EquipeProfissionalType())
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Plugged\DefaultBundle\Entity\Equipe'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'plugged_defaultbundle_equipe';
    }
}
