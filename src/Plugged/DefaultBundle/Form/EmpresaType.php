<?php

namespace Plugged\DefaultBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EmpresaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nome','text',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'Nome',
                'required' => true
                
            ))
            ->add('cnpj','text',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'CNPJ',
                'required' => true
                
            ))
            ->add('cidade','text',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'Cidade',
                'required' => true
                
            ))
            ->add('estado','text',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'Estado',
                'required' => true
                
            ))
            ->add('porte','text',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'porte',
                'required' => true
                
            ))
            ->add('seguimento','text',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'Segmento',
                'required' => true
                
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Plugged\DefaultBundle\Entity\Empresa'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'plugged_defaultbundle_empresa';
    }
}
