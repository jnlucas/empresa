<?php

namespace Plugged\DefaultBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProfissionalType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('nome', 'text',array(
            'attr' => array(
                'class' => 'form-control'
            ),
            'label' => 'Profissional'
        ))
        ->add('nickname', 'text',array(
            'attr' => array(
                'class' => 'form-control'
            ),
            'label' => 'Apelido'
        ))
            ->add('email', 'text',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'Email'
            ))
            ->add('telefone', 'text',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'Telefone'
            ))
            ->add('cpf', 'text',array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'CPF'
            ))


            ->add('nascimento', 'datetime', [
                'attr' => [
                    'class' => 'form-control date',
                    'data-rule-required' => false,
                    'data-date-format'=>"dd/mm/yyyy"
                ],
                'label' => 'Data De Nascimento',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'required' => true,
            ])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Plugged\DefaultBundle\Entity\Profissional'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'plugged_defaultbundle_profissional';
    }
}
