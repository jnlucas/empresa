<?php
namespace Plugged\DefaultBundle\Services;
use Plugged\DefaultBundle\Entity\AtividadeMeta;
use Plugged\DefaultBundle\Entity\AtividadeMetaProfissional;


class ScoreService{

    private $em;
    private $container;

    public function __construct($em,$container){

        $this->em = $em;
        $this->container = $container;

    }


    public function getScore($request,$profissionalId,$metaId){



        $profissional = $this->em->getRepository('DefaultBundle:Profissional')->find($profissionalId);

        $meta = $this->em->getRepository('DefaultBundle:Meta')->find($metaId);


        $equipeProfissional = $this->em->getRepository('DefaultBundle:EquipeProfissional')->findOneBy(['profissionalId' => $profissionalId, 'status' => true]);

        $equipeProfissionais = $this->em->getRepository('DefaultBundle:EquipeProfissional')->findBy(['equipeId' => $equipeProfissional->getEquipeId(), 'status' => true]);


        $equipe = $this->em->getRepository('DefaultBundle:Equipe')->find($equipeProfissionais[0]->getEquipeId());


        $profissionais = [];

        foreach($equipeProfissionais as $profissional){
            $profissionais[] = $profissional->getProfissionalId();
        }

        $avaliacoesProfissional = $this->em->getRepository('DefaultBundle:ProfissionalAvaliacao')->findBy(['profissionalId' => $profissionais]);

        $atividadesProfissional = $this->em->getRepository('DefaultBundle:AtividadeMetaProfissional')->findBy(['profissionalId' => $profissionais]);

        $pontosProfissionais = [];

        $pontos = [];

        usort($avaliacoesProfissional, function($a,$b){
             return $a->getData()<$b->getData();
        });

        foreach($avaliacoesProfissional as $avaliacao)
        {
            if($avaliacao->getProfissionalId()){
                if(!isset($pontos[$avaliacao->getProfissionalId()->getId()])){
                    $pontos[$avaliacao->getProfissionalId()->getId()] = 0;
                }
                $pontos[$avaliacao->getProfissionalId()->getId()] = $pontos[$avaliacao->getProfissionalId()->getId()] + $avaliacao->getPontuacao();
                $pontosProfissionais[$avaliacao->getProfissionalId()->getId()]['pontos'] = $pontos[$avaliacao->getProfissionalId()->getId()];
                $pontosProfissionais[$avaliacao->getProfissionalId()->getId()]['profissionalId'] = $avaliacao->getProfissionalId()->getId();
                $pontosProfissionais[$avaliacao->getProfissionalId()->getId()]['profissional'] = $avaliacao->getProfissionalId()->getNome();
                $pontosProfissionais[$avaliacao->getProfissionalId()->getId()]['meta'] = $meta->getPontos();
                $pontosProfissionais[$avaliacao->getProfissionalId()->getId()]['vencimento'] = $meta->getPeriodoFim()->format('d-m-Y');
                $pontosProfissionais[$avaliacao->getProfissionalId()->getId()]['equipe'] = $equipe->getDescricao();
                $pontosProfissionais[$avaliacao->getProfissionalId()->getId()]['avaliacoes'][] = array(
                    'superiorImediato' => $avaliacao->getSuperiorImediato(),
                    'pontos' => $avaliacao->getPontuacao(),
                    'observacao' => $avaliacao->getObservacao(),
                    'data' => $avaliacao->getData()->format('d-m-Y'),
                    'dataOriginal' => $avaliacao->getData(),

                );
            }

        }

        foreach($atividadesProfissional as $avaliacao)
        {
            if(!isset($pontos[$avaliacao->getProfissionalId()->getId()])){
                $pontos[$avaliacao->getProfissionalId()->getId()] = 0;
            }
            $pontos[$avaliacao->getProfissionalId()->getId()] += ($avaliacao->getAtividadeMetaId()->getStatus() == 2) ? $avaliacao->getAtividadeMetaId()->getPontos():0;
            $pontosProfissionais[$avaliacao->getProfissionalId()->getId()]['pontos'] = $pontos[$avaliacao->getProfissionalId()->getId()];
            $pontosProfissionais[$avaliacao->getProfissionalId()->getId()]['foto'] = 'http://api.14mob.com/bundles/default/uploads/'.$avaliacao->getProfissionalId()->getFoto();

            $pontosProfissionais[$avaliacao->getProfissionalId()->getId()]['profissional'] = $avaliacao->getProfissionalId()->getNome();
            $pontosProfissionais[$avaliacao->getProfissionalId()->getId()]['meta'] = $meta->getPontos();
            $pontosProfissionais[$avaliacao->getProfissionalId()->getId()]['vencimento'] = $meta->getPeriodoFim()->format('d-m-Y');

            $pontosProfissionais[$avaliacao->getProfissionalId()->getId()]['equipe'] = $equipe->getDescricao();
        }

        usort($pontosProfissionais, function($a,$b){
             return $a['pontos']<$b['pontos'];
        });



        return ($pontosProfissionais);

    }

    public function processaScore(){

        $importacoes = $this->em->getRepository("DefaultBundle:Importacao")->findAll();

        foreach($importacoes as $ip){
            $atividade = $this->em->getRepository("DefaultBundle:AtividadeMeta")->findOneBy(["protocolo" => $ip->getProtocolo()]);
            if($atividade){
                $atividadesProfissional = $this->em->getRepository("DefaultBundle:AtividadeMetaProfissional")
                ->findBy([
                    "atividadeMetaId" => $atividade->getId()
                ]);

                if($atividadesProfissional){
                    foreach($atividadesProfissional as $p){
                        $this->em->remove($p);
                    }
                }
            }

        }
        $this->em->flush();

        foreach($importacoes as $importacao){

            if($importacao->getSituacao() == "entregue")
                $status = 2;
            if($importacao->getSituacao() == "aberto")
                $status = 1;
            if($importacao->getSituacao() == "bloqueado")
                $status = 0;


            $meta = $this->em->getRepository("DefaultBundle:Meta")->find($importacao->getMeta());
            $atividade = $this->em->getRepository("DefaultBundle:AtividadeMeta")->findOneBy(["protocolo" => $importacao->getProtocolo()]);
            $profissional = $this->em->getRepository("DefaultBundle:Profissional")->findOneBy(["cpf" => $importacao->getProfissional()]);
            if(!$atividade){
                $atividade = new AtividadeMeta();
                $atividade->setDescricao($importacao->getAtividade())
                ->setProtocolo($importacao->getProtocolo())
                ->setPontos($importacao->getPontos())
                ->setVencimento($importacao->getVencimento())
                ->setDataCadastro(new \DateTime("now"))
                ->setStatus($status)
                ->setMetaId($meta);

                $this->em->persist($atividade);
            }else{
                $atividade->setDescricao($importacao->getAtividade())
                ->setProtocolo($importacao->getProtocolo())
                ->setPontos($importacao->getPontos())
                ->setVencimento($importacao->getVencimento())
                ->setDataCadastro(new \DateTime("now"))
                ->setStatus($status)
                ->setMetaId($meta);


            }

            $this->em->flush($atividade);

            if($profissional){
                $atividadesProfissional = $this->em->getRepository("DefaultBundle:AtividadeMetaProfissional")
                ->findBy([
                    "atividadeMetaId" => $atividade->getId()
                ]);

                

                $atividadeProfissional = new AtividadeMetaProfissional();

                $atividadeProfissional->setProfissionalId($profissional)
                ->setAtividadeMetaId($atividade)
                ->setStatus(1)
                ->setDataCadastro(new \DateTime("now"));

                $this->em->persist($atividadeProfissional);

                $this->em->flush();

            }





        }
        foreach($importacoes as $import){
            $this->em->remove($import);

        }
        $this->em->flush();

    }

}


?>
