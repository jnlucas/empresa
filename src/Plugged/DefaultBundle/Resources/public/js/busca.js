$(function() {
	$('body').on('click','.buscar',function(e) {
		e.preventDefault();
		var pane = $('.lojas');
		var formData = ($('#pesquisa').serialize());
		var dados = {
			marca: $('#marca').val(),
			modelo: $('#modelo').val(),
			motor: $('#motor').val(),
			minPrice: $('#minPrice').val(),
			maxPrice: $('#maxPrice').val(),
			minAno: $('#minAno').val(),
			maxAno: $('#maxAno').val(),
			minKm: $('#minKm').val(),
			maxKm: $('#maxKm').val(),
			itens: decodeURI(formData),
		};
		
		// ajax load from data-url
		$("#allAds").html('Aguarde ...');
		var body = $("html, body");
			body.stop().animate({scrollTop:0}, '500', 'swing', function() { 
			   
			});
		$("#allAds").load(url,{dados:dados}, function(result) {
			console.log(result);
			if(!result){
				$("#allAds").html('nenhum resultado encontrado!');
			}
			pane.tab('show');
			
			
			// ajax pre-request callback function
			$('.tooltipHere').tooltip('hide');
			$('.grid-view').click(function(e) {
				$(function() {
					$('.item-list').matchHeight();
					$.fn.matchHeight._apply('.item-list');
				});
			});

		});
	});

});

